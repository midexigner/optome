<?php 
require_once 'admin/Common.php';
require_once 'include/head.php';
require_once 'include/header.php';
$packagesHeadBgQ = mysql_query('SELECT * FROM `banners` WHERE ID = 11');
$packageHeadRowBg = mysql_fetch_assoc($packagesHeadBgQ);
?>

<style>

  .login-breadscrumb {
    background-image: url(<?php echo SITE_URL .'/admin/'.DIR_BANNERS.$packageHeadRowBg['Image'] ?>) !important;
}
</style>
<section id="apus-breadscrumb" class="apus-breadscrumb login-breadscrumb">
         <div class="container">
            <div class="row">
               <div class="wrapper-breads">
                  <div class="breadscrumb-inner">
                     <ol class="breadcrumb">
                        <li><a href="index.php">Home</a>  </li>
                        <li class="active">Login</li>
                     </ol>
                     <h2 class="bread-title">Login</h2>
                  </div>
               </div>
            </div>
         </div>
      </section>


      <section class="login">
<div class="container">

<div class="row">
    <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
    <form role="form">
      <fieldset>
        <h2>Please Log-In</h2>
        <p>To access your optome account</p>
        <hr class="colorgraph">
        <div class="form-group">
                    <input type="text" name="username" id="username" class="form-control input-lg" placeholder="Username" data-error="Please insert the Username">
                       <div class="help-block with-errors"></div>
        </div>
        <div class="form-group">
                    <input type="password" name="password" id="password" class="form-control input-lg" placeholder="Password" data-error="Please insert the Password">
                       <div class="help-block with-errors"></div>
        </div>
        <span class="button-checkbox">
          <button type="button" class="btn btn-rem" data-color="info">Remember Me</button>
                    <input type="checkbox" name="remember_me" id="remember_me" checked="checked" class="hidden">
          <a href="" class="btn btn-link pull-right">Forgot Password?</a>
        </span>
        <hr class="colorgraph">
        <div class="row">
          <div class="col-xs-6 col-sm-6 col-md-6 col-md-offset-3">
                        <input type="submit" class="btn btn-lg btn-outline btn-block" value="Sign In">
          </div>
          
        </div>
      </fieldset>
    </form>
  </div>
</div>

</div>
</section>

<?php 

require_once 'include/footer.php';
require_once 'include/foot.php';

 ?>
  