-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 05, 2018 at 06:00 AM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `optome`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE IF NOT EXISTS `about` (
  `ID` int(11) NOT NULL,
  `Text` text NOT NULL,
  `Heading` text NOT NULL,
  `Image1` text NOT NULL,
  `Image2` text NOT NULL,
  `Image` text NOT NULL,
  `PerformedBy` int(11) NOT NULL,
  `DateModified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`ID`, `Text`, `Heading`, `Image1`, `Image2`, `Image`, `PerformedBy`, `DateModified`) VALUES
(1, '<p>OptoMe is a strong and stable broadband, offering fiber to the home (FTTH) services on GPON technology powered by one of the largest Internet &amp; data services providing company in Pakistan, Connect Communication (Pvt.) Ltd.</p>\r\n<p>We ensure to enable our customers with fiber to the home (FTTH) for the residential broadband. Fiber to the home (FTTH) is the delivery of a communications signal over optical fiber all the way to the home. In order to provide seamless connectivity as copper infrastructure and coaxial cable are replaced by optical fiber. FTTH is relatively new and fast growing solution for providing vastly higher bandwidth to consumers for the high quality Internet and HD quality TV and Voice.</p>', 'Welcome To OPTOME', 'about1.png', 'about2.jpg', '1-0.jpg,1-1.jpg,1-2.jpg', 4, '2017-12-26 16:36:27');

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE IF NOT EXISTS `banners` (
  `ID` int(11) NOT NULL,
  `Heading` varchar(255) NOT NULL,
  `Image` text NOT NULL,
  `HomePage` int(11) NOT NULL,
  `Status` tinyint(1) NOT NULL,
  `Sort` int(11) NOT NULL,
  `PerformedBy` int(11) NOT NULL,
  `DateAdded` datetime NOT NULL,
  `DateModified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`ID`, `Heading`, `Image`, `HomePage`, `Status`, `Sort`, `PerformedBy`, `DateAdded`, `DateModified`) VALUES
(5, 'Home Packages', '5.jpg', 1, 1, 0, 4, '2017-11-23 14:32:21', '2017-12-22 17:15:17'),
(6, 'Parallex company page', '6.png', 0, 1, 0, 4, '2017-11-23 15:06:32', '2017-12-22 17:35:59'),
(10, 'Parallex package page', '10.jpg', 0, 1, 0, 4, '2017-11-23 16:00:15', '2017-12-22 17:36:35'),
(11, 'Pages head', '11.png', 0, 1, 0, 4, '2017-11-23 16:00:29', '2017-12-22 17:37:33');

-- --------------------------------------------------------

--
-- Table structure for table `career`
--

CREATE TABLE IF NOT EXISTS `career` (
  `ID` int(11) NOT NULL,
  `Name` text NOT NULL,
  `File` text NOT NULL,
  `Des` text NOT NULL,
  `email` text NOT NULL,
  `DateAdded` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `career`
--

INSERT INTO `career` (`ID`, `Name`, `File`, `Des`, `email`, `DateAdded`) VALUES
(1, 'test', 'careers/lva1-app6892.docx', 'asssssssssssss', 'admin@admin.com', '2017-11-24 15:42:08');

-- --------------------------------------------------------

--
-- Table structure for table `careers`
--

CREATE TABLE IF NOT EXISTS `careers` (
  `ID` int(11) NOT NULL,
  `Heading` text NOT NULL,
  `Text` text NOT NULL,
  `Description` text NOT NULL,
  `Status` tinyint(1) NOT NULL,
  `ShowOnHome` int(11) NOT NULL,
  `Sort` int(11) NOT NULL,
  `PerformedBy` int(11) NOT NULL,
  `DateAdded` datetime NOT NULL,
  `DateModified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `careers`
--

INSERT INTO `careers` (`ID`, `Heading`, `Text`, `Description`, `Status`, `ShowOnHome`, `Sort`, `PerformedBy`, `DateAdded`, `DateModified`) VALUES
(24, 'Female Receptionist', 'We are looking to hire a full time FEMALE receptionist / personal secretary. Candidate must be Female with minimum Education: A-Levels/FSC/Inter/Bachelors.', '1. Handles Incoming and Outgoing Call\n\n2. Monitoring of System Event and its follow-up with Customer.\n\n3. Providing services to customer with relevant information.\n\n4. Managing ERP Applicaiton.\n\n\n\nSpecification\n\n1. Candidate must have good communication skills\n\n2. Knowledge of Microsoft application\n\n3. Must be attentive, sharp and proactive\n\n\n\nRewards and Benefits\n\nHealth Insurance\n\nLeaves\n\nMedical', 1, 0, 0, 4, '2016-10-21 14:04:02', '2016-10-21 14:11:58'),
(25, 'Coordinator to CEO', 'Reporting to CEO to daily operations work or assist to CEO. Skills. Must have MS Office skills.', '1. Handles Incoming and Outgoing Call\n\n2. Monitoring of System Event and its follow-up with Customer.\n\n3. Providing services to customer with relevant information.\n\n4. Managing ERP Applicaiton.\n\n\n\nSpecification\n\n1. Candidate must have good communication skills\n\n2. Knowledge of Microsoft application\n\n3. Must be attentive, sharp and proactive\n\n\n\nRewards and Benefits\n\nHealth Insurance\n\nLeaves\n\nMedical', 1, 0, 2, 4, '2016-10-21 14:16:52', '0000-00-00 00:00:00'),
(26, 'Control Room Officer', 'Handles Incoming and Outgoing Call and Monitoring of System Event and its follow-up with Customer. Also providing services to customer with relevant information. Managing ERP Applicaiton', '1. Handles Incoming and Outgoing Call\n\n2. Monitoring of System Event and its follow-up with Customer.\n\n3. Providing services to customer with relevant information.\n\n4. Managing ERP Applicaiton.\n\n\n\nSpecification\n\n1. Candidate must have good communication skills\n\n2. Knowledge of Microsoft application\n\n3. Must be attentive, sharp and proactive\n\n\n\nRewards and Benefits\n\nHealth Insurance\n\nLeaves\n\nMedical', 1, 0, 1, 4, '2016-10-21 14:17:26', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE IF NOT EXISTS `clients` (
  `ID` int(11) NOT NULL,
  `Name` text NOT NULL,
  `Heading` text NOT NULL,
  `Slider` text NOT NULL,
  `URL` text NOT NULL,
  `Status` tinyint(1) NOT NULL,
  `HomePage` int(11) NOT NULL,
  `Sort` int(11) NOT NULL,
  `PerformedBy` int(11) NOT NULL,
  `DateAdded` datetime NOT NULL,
  `DateModified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`ID`, `Name`, `Heading`, `Slider`, `URL`, `Status`, `HomePage`, `Sort`, `PerformedBy`, `DateAdded`, `DateModified`) VALUES
(1, '', '', '1.jpg', '', 1, 1, 0, 4, '2017-11-23 16:37:34', '0000-00-00 00:00:00'),
(2, '', '', '2.jpg', '', 1, 1, 0, 4, '2017-11-23 16:41:10', '0000-00-00 00:00:00'),
(3, '', '', '3.jpg', '', 1, 1, 0, 4, '2017-11-23 16:43:06', '2017-11-23 16:55:44'),
(4, '', '', '4.png', '', 1, 0, 0, 4, '2017-11-23 16:50:14', '2017-11-23 16:59:34'),
(5, '', '', '5.jpg', '', 1, 0, 0, 4, '2017-11-23 16:50:23', '2017-11-23 16:58:19'),
(6, '', '', '6.png', '', 1, 0, 0, 4, '2017-11-23 16:50:38', '2017-11-23 16:58:09');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE IF NOT EXISTS `contacts` (
  `ID` int(11) NOT NULL,
  `Name` text NOT NULL,
  `Email` text NOT NULL,
  `Phone` varchar(190) NOT NULL,
  `Subject` text NOT NULL,
  `Message` text NOT NULL,
  `DateAdded` datetime NOT NULL,
  `DateModified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`ID`, `Name`, `Email`, `Phone`, `Subject`, `Message`, `DateAdded`, `DateModified`) VALUES
(1, 'Garrett Garza', 'cesasak@yahoo.com', '+238-33-9672912', 'Amet consequatur id excepteur velit nostrum aspernatur eius ea nihil qui provident reprehenderit dolore', 'Ut qui mollit odit et perspiciatis vitae eiusmod', '2017-12-23 10:39:37', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `deals`
--

CREATE TABLE IF NOT EXISTS `deals` (
  `ID` int(11) NOT NULL,
  `Heading` text NOT NULL,
  `Text` text NOT NULL,
  `Image` text NOT NULL,
  `Status` tinyint(1) NOT NULL,
  `ShowOnHome` int(11) NOT NULL,
  `Sort` int(11) NOT NULL,
  `PerformedBy` int(11) NOT NULL,
  `DateAdded` datetime NOT NULL,
  `DateModified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deals`
--

INSERT INTO `deals` (`ID`, `Heading`, `Text`, `Image`, `Status`, `ShowOnHome`, `Sort`, `PerformedBy`, `DateAdded`, `DateModified`) VALUES
(24, 'Our Hi Tea is Happy Hours', 'Ramada wishes nothing more than to offer our guests with cherished memories and loved memories and what better way than to enjoy a hot cup of coffee with someone you care for. Savour the essence of freshly brewed tea in just Rs. 499+tax with more than 25 Dishes. For further details and booking call us at 021 35244400', '24.jpg', 1, 0, 0, 4, '2016-12-22 10:35:08', '0000-00-00 00:00:00'),
(25, 'Midnight deal', 'The feeling of joy associated with a freshly cooked meal is not bound by time. If hunger strikes at midnight then Ramada has just the offer for you. The best deals on the best meals for you made by our expert chefs. For more Contact us on 021-35244400  ', '25.jpg', 1, 0, 1, 4, '2016-12-22 10:35:48', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `downloads`
--

CREATE TABLE IF NOT EXISTS `downloads` (
  `ID` int(11) NOT NULL,
  `Name` text NOT NULL,
  `Heading` text NOT NULL,
  `Text` text NOT NULL,
  `Slider` text NOT NULL,
  `CornerTag` varchar(255) DEFAULT NULL,
  `RightImage` varchar(255) DEFAULT NULL,
  `URL` text NOT NULL,
  `Status` tinyint(1) NOT NULL,
  `Sort` int(11) NOT NULL,
  `PerformedBy` int(11) NOT NULL,
  `DateAdded` datetime NOT NULL,
  `DateModified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `downloads`
--

INSERT INTO `downloads` (`ID`, `Name`, `Heading`, `Text`, `Slider`, `CornerTag`, `RightImage`, `URL`, `Status`, `Sort`, `PerformedBy`, `DateAdded`, `DateModified`) VALUES
(57, '', 'Efficient Energy Management', '', '57.jpg', '57_1.pdf', NULL, '', 1, 0, 4, '2017-11-23 17:26:37', '0000-00-00 00:00:00'),
(58, '', 'Business Automation', '', '58.jpg', '58_1.pdf', NULL, '', 1, 0, 4, '2017-11-23 17:28:25', '0000-00-00 00:00:00'),
(59, '', 'Smart Home', '', '59.jpg', '59_1.pdf', NULL, '', 1, 0, 4, '2017-11-23 17:29:05', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE IF NOT EXISTS `faq` (
  `ID` int(11) NOT NULL,
  `Name` text NOT NULL,
  `Body` text NOT NULL,
  `URL` text NOT NULL,
  `Status` tinyint(1) NOT NULL,
  `DateAdded` datetime NOT NULL,
  `DateModified` datetime NOT NULL,
  `PerformedBy` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faq`
--

INSERT INTO `faq` (`ID`, `Name`, `Body`, `URL`, `Status`, `DateAdded`, `DateModified`, `PerformedBy`) VALUES
(7, 'What is Optome Fibre??', 'It''s our fibre optic broadband service, offering superfast download speeds of up to 76Mbps.\r\n\r\nThe actual speed you''ll get depends on the package you choose, the quality of your phone line and your home wiring.', '', 1, '2017-12-22 16:37:17', '0000-00-00 00:00:00', 4),
(8, 'Will speeds go up and down?', 'Yes, fibre broadband uses the same technology (Dynamic Line Management) as standard broadband to give you the best possible service.\r\n\r\nYou''ll probably see your speed vary over the first 10 days, as the broadband system runs tests to find the best speed for your service. This can cause your speeds to go up and down. You may even get disconnected a few times. Don''t worry, this doesn''t mean there''s a problem, so please bear with it.', '', 1, '2017-12-22 16:37:17', '0000-00-00 00:00:00', 4),
(9, 'Fibre availability: How can I find out if it''s available in my area?', 'Go to our homepage and click ''Check Availability'' on any of our broadband products. Enter your landline phone and/or address details and we can let you know about fibre availability in your area.\r\n\r\nNote: There are some reasons why you may not be able to get Plusnet Fibre, even though you live in a fibre optic broadband enabled area.\r\n\r\n If you''re connected directly to a telephone exchange, rather than via a green cabinet\r\n If your line is too far from your nearest green cabinet to support a stable fibre optic broadband service\r\n Work hasn''t yet been done at your nearest green cabinet\r\n Your green cabinet isn''t suitable for fibre optic cabling', '', 1, '2017-12-22 16:39:05', '0000-00-00 00:00:00', 4),
(10, 'How fast will my broadband be?', 'You''ll get the fastest speed that your telephone line can reliably support. Select the service you''re interested in and click ''start your order'' to get a speed estimate. Speeds are available as follows:\r\n\r\n Up to 38Mbps or Up to 76Mbps for Fibre (FTTC) enabled areas depending on the package you choose\r\n Up to 17Mbps for ADSL2+ enabled areas\r\n Up to 7Mbps service in other areas\r\nYour speed will be presented as a range of values in megabits per second (Mbps). The actual speed you get will usually be within these values and may vary depending on line conditions. If you find that your speed is much slower than the lowest estimated value you should call us on 0800 432 0200 and we''ll work with you to get the best possible speed for your line.', '', 1, '2017-12-22 16:39:05', '0000-00-00 00:00:00', 4),
(11, 'Is support included?', 'We provide award-winning support 365 days a year, either online or over the phone. It''s free to call from any UK landline or mobile.\r\n\r\nThere are a number of ways to contact us. Our help articles, FAQs sand Community Site forums will also give you quick answers to your questions.', '', 1, '2017-12-22 16:39:30', '0000-00-00 00:00:00', 4);

-- --------------------------------------------------------

--
-- Table structure for table `heaingtext`
--

CREATE TABLE IF NOT EXISTS `heaingtext` (
  `ID` int(11) NOT NULL,
  `Heading` varchar(255) NOT NULL,
  `Image` text NOT NULL,
  `Text` text NOT NULL,
  `text2` text NOT NULL,
  `HomePage` int(11) NOT NULL,
  `Status` tinyint(1) NOT NULL,
  `Sort` int(11) NOT NULL,
  `PerformedBy` int(11) NOT NULL,
  `DateAdded` datetime NOT NULL,
  `DateModified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `heaingtext`
--

INSERT INTO `heaingtext` (`ID`, `Heading`, `Image`, `Text`, `text2`, `HomePage`, `Status`, `Sort`, `PerformedBy`, `DateAdded`, `DateModified`) VALUES
(5, 'Committed to providing top-notch fiber broadband services to ensure 100% customer satisfaction', '5.', '&nbsp;', '', 1, 1, 0, 4, '2017-11-23 14:32:21', '2017-12-28 14:53:08'),
(6, 'SPEED', '6.', 'HIGH BANDWIDTH', 'RELIABILITY', 1, 1, 0, 4, '2017-11-23 15:06:32', '2017-12-28 17:13:22'),
(10, 'HIGH', '10.png', 'DEFINITION TV', 'CONSISTENCY', 1, 1, 0, 4, '2017-11-23 16:00:15', '2017-12-27 12:29:33'),
(12, 'One-stop solution for your reliable FTTH connection with best optimal routes', '12.jpg', '&nbsp;', '', 1, 1, 0, 4, '2017-11-23 16:00:41', '2017-12-26 16:30:14'),
(15, 'Are you Muddled? Opt for Our â€˜OPTOMEâ€™ Fiber Broadband Service', '12.jpg', 'Â We are focused on providing services with the highest level of customer satisfaction & we will do everything we can to meet your expectations.', '', 0, 1, 0, 4, '2017-11-23 16:00:41', '2017-12-28 14:28:34'),
(16, 'Internet', '16.', 'Â &nbsp;', '', 0, 1, 0, 4, '2017-11-23 16:00:41', '2017-12-28 15:31:57'),
(17, 'Television', '17.', '&nbsp;', '', 0, 1, 0, 4, '2017-11-23 16:00:41', '2017-12-28 15:31:44'),
(18, 'Phone', '18.', '&nbsp;', '', 1, 1, 0, 4, '2017-11-23 16:00:41', '2017-12-28 15:32:40'),
(19, 'Addon', '', '<dt><span class="icon-holder"><img src="assets/img/icon-phone.png" alt="image description"></span> Phone</dt>\n                                    <dd>Rs 150/<sub>mo</sub></dd>\n                                    <dt><span class="icon-holder"><img src="assets/img/icon-static-ip.png" alt="image description"></span> Static IP</dt>\n                                    <dd>Rs 1,000/<sub>mo</sub></dd>\n                                  \n                                    <dt><span class="icon-holder"><img src="assets/img/icon-additional-tv.png" alt="image description"></span> Additional TV</dt>\n                                    <dd>Rs 200/<sub>mo</sub></dd>', '<dt>Landline Calling <sub>(Local)</sub></dt>                                     <dd>Rs 1.10<sub>per/min</sub></dd>                                     <dt>Landline Calling <sub>(Nationwide)</sub></dt>                                     <dd>Rs 1.30<sub>per/min</sub></dd>                                     <dt>Mobile Calling <sub>(Nationwide)</sub></dt>                                     <dd>Rs 2.25<sub>per/min</sub></dd>                                     <dt>On-net Calling <sub>(Local & Nationwide)</sub></dt>                                     <dd>Free</dd>', 0, 1, 0, 4, '2017-11-23 16:00:41', '2018-01-02 18:18:01');

-- --------------------------------------------------------

--
-- Table structure for table `home`
--

CREATE TABLE IF NOT EXISTS `home` (
  `ID` int(11) NOT NULL,
  `Text` text NOT NULL,
  `Heading` text NOT NULL,
  `Para` text NOT NULL,
  `Image1` text NOT NULL,
  `Image2` text NOT NULL,
  `Image` text NOT NULL,
  `PerformedBy` int(11) NOT NULL,
  `DateModified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home`
--

INSERT INTO `home` (`ID`, `Text`, `Heading`, `Para`, `Image1`, `Image2`, `Image`, `PerformedBy`, `DateModified`) VALUES
(1, 'Cemented with the ideology of customer care and instilled with desire to achieve excellence, Ramada Creek is committed towards bolstering the unique bond it has with the guests that stay at the hotel. When you check into Ramada Creek, our carefully crafted facilities are always just a command away so that you make the most of your time.', 'AVAIL OUR LUXURIOUS SERVICES', 'Built with true marksmanship and nurtured with divine customer care, Ramada Creek has left competition behind in terms of excellence and sophistication. ', 'home1.jpg', 'home2.jpg', '1-0.jpg,1-1.jpg,1-2.jpg', 4, '2016-12-21 16:42:16');

-- --------------------------------------------------------

--
-- Table structure for table `multimedia`
--

CREATE TABLE IF NOT EXISTS `multimedia` (
  `ID` int(11) NOT NULL,
  `Name` text NOT NULL,
  `File` text NOT NULL,
  `Type` tinyint(1) NOT NULL,
  `DateAdded` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `multimedia`
--

INSERT INTO `multimedia` (`ID`, `Name`, `File`, `Type`, `DateAdded`) VALUES
(22, 'Icon Phone', '22.png', 1, '2018-01-02 18:02:23'),
(23, 'icon-static-ip', '23.png', 1, '2018-01-02 18:07:09'),
(24, 'icon-additional-tv', '24.png', 1, '2018-01-02 18:07:30');

-- --------------------------------------------------------

--
-- Table structure for table `newsletters`
--

CREATE TABLE IF NOT EXISTS `newsletters` (
  `ID` int(11) NOT NULL,
  `FirstName` text NOT NULL,
  `LastName` text NOT NULL,
  `EmailAddress` text NOT NULL,
  `Status` int(11) NOT NULL DEFAULT '1',
  `DateAdded` datetime NOT NULL,
  `DateModified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `newsletters`
--

INSERT INTO `newsletters` (`ID`, `FirstName`, `LastName`, `EmailAddress`, `Status`, `DateAdded`, `DateModified`) VALUES
(5, '', '', 'admin@admin.com', 1, '2017-11-24 14:49:05', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `ourstory`
--

CREATE TABLE IF NOT EXISTS `ourstory` (
  `ID` int(11) NOT NULL,
  `Image` text NOT NULL,
  `Status` tinyint(1) NOT NULL,
  `url` text NOT NULL,
  `Sort` int(11) NOT NULL,
  `PerformedBy` int(11) NOT NULL,
  `DateAdded` datetime NOT NULL,
  `DateModified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ourstory`
--

INSERT INTO `ourstory` (`ID`, `Image`, `Status`, `url`, `Sort`, `PerformedBy`, `DateAdded`, `DateModified`) VALUES
(1, '1.png', 1, 'what-we-offer.php#internet-us', 0, 4, '2017-10-10 22:33:21', '2017-12-23 15:32:37'),
(2, '2.png', 1, 'what-we-offer.php#tv', 0, 4, '2017-10-10 22:33:21', '2017-12-26 09:21:50'),
(3, '3.png', 1, 'what-we-offer.php#telephone', 0, 4, '2017-10-10 22:33:21', '2017-12-23 16:03:04');

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE IF NOT EXISTS `packages` (
  `ID` int(11) NOT NULL,
  `Name` text NOT NULL,
  `subheading` text NOT NULL,
  `Image` text NOT NULL,
  `Status` tinyint(1) NOT NULL,
  `OfferPrice` float(11,2) NOT NULL,
  `OldPrice` float(11,2) NOT NULL,
  `Details` text NOT NULL,
  `Sort` int(11) NOT NULL,
  `DateAdded` datetime NOT NULL,
  `DateModified` datetime NOT NULL,
  `PerformedBy` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`ID`, `Name`, `subheading`, `Image`, `Status`, `OfferPrice`, `OldPrice`, `Details`, `Sort`, `DateAdded`, `DateModified`, `PerformedBy`) VALUES
(9, 'Single 5', '5 Mbps Fiber Broadband', '9.png', 1, 1700.00, 0.00, '<div class="description">Onetime instaltion charges Rs. 10,000/- <br />Rates are exclusive of taxes<br /> Add-on service charges Rs. 350/- <br /><br />*Fair usage policy apply</div>\r\n<hr />\r\n<p class="para">Fiber Broadband Service&nbsp;</p>', 0, '2017-12-26 10:16:13', '2017-12-28 17:37:25', 4),
(10, 'Double1', '11 Mb Fiber Broadband', '10.png', 1, 2000.00, 0.00, '<div class="description">Onetime instaltion charges Rs. 10,000/- <br />Rates are exclusive of taxes<br /> Add-on service charges Rs. 350/- <br /><br />*Fair usage policy apply</div>\r\n<hr />\r\n<p class="para">Fiber Broadband Service&nbsp;</p>', 0, '2017-12-26 10:24:48', '2017-12-26 07:15:11', 4),
(11, 'Double2', '22 Mbps Fiber Broadband', '11.png', 1, 2800.00, 0.00, '<div class="description">Onetime instaltion charges Rs. 10,000/- <br />Rates are exclusive of taxes<br /> Add-on service charges Rs. 350/- <br /><br />*Fair usage policy apply</div>\r\n<hr />\r\n<p class="para">Fiber Broadband Service&nbsp;</p>', 0, '2017-12-26 10:27:08', '2017-12-26 07:20:40', 4),
(12, 'Double3', '33 Mbps Fiber Broadband', '12.png', 1, 3600.00, 0.00, '<div class="description">Onetime instaltion charges Rs. 10,000/- <br />Rates are exclusive of taxes<br /> Add-on service charges Rs. 350/- <br /><br />*Fair usage policy apply</div>\r\n<hr />\r\n<p class="para">Fiber Broadband Service&nbsp;</p>', 0, '2017-12-26 10:28:31', '2017-12-26 07:22:25', 4),
(13, 'Double4', '44 Mbps Fiber Broadband', '13.png', 1, 4400.00, 0.00, '<div class="description">Onetime instaltion charges Rs. 10,000/- <br />Rates are exclusive of taxes<br /> Add-on service charges Rs. 350/- <br /><br />*Fair usage policy apply</div>\r\n<hr />\r\n<p class="para">Fiber Broadband Service&nbsp;</p>', 0, '2017-12-26 10:29:21', '2017-12-26 07:23:19', 4),
(14, 'Double5', '55 Mbps Fiber Broadband', '14.png', 1, 5200.00, 0.00, '<div class="description">Onetime instaltion charges Rs. 10,000/- <br />Rates are exclusive of taxes<br /> Add-on service charges Rs. 350/- <br /><br />*Fair usage policy apply</div>\r\n<hr />\r\n<p class="para">Fiber Broadband Service&nbsp;</p>', 0, '2017-12-26 10:29:57', '2017-12-26 07:23:58', 4),
(15, 'Double6', '66 Mbps Fiber Broadband', '15.png', 1, 6000.00, 0.00, '<div class="description">Onetime instaltion charges Rs. 10,000/- <br />Rates are exclusive of taxes<br /> Add-on service charges Rs. 350/- <br /><br />*Fair usage policy apply</div>\r\n<hr />\r\n<p class="para">Fiber Broadband Service&nbsp;</p>', 0, '2017-12-26 10:31:11', '2017-12-26 07:24:32', 4),
(16, 'Double7', '77 Mbps Fiber Broadband', '16.png', 1, 6800.00, 0.00, '<div class="description">Onetime instaltion charges Rs. 10,000/- <br />Rates are exclusive of taxes<br /> Add-on service charges Rs. 350/- <br /><br />*Fair usage policy apply</div>\r\n<hr />\r\n<p class="para">Fiber Broadband Service&nbsp;</p>', 0, '2017-12-26 10:32:57', '2017-12-26 07:25:07', 4),
(17, 'Double8', '88 Mbps Fiber Broadband', '17.png', 1, 7600.00, 0.00, '<div class="description">Onetime instaltion charges Rs. 10,000/- <br />Rates are exclusive of taxes<br /> Add-on service charges Rs. 350/- <br /><br />*Fair usage policy apply</div>\r\n<hr />\r\n<p class="para">Fiber Broadband Service&nbsp;</p>', 0, '2017-12-26 10:33:48', '2017-12-26 07:31:07', 4),
(18, 'Double9', '99 Mbps Fiber Broadband', '18.png', 1, 8400.00, 0.00, '<div class="description">Onetime instaltion charges Rs. 10,000/- <br />Rates are exclusive of taxes<br /> Add-on service charges Rs. 350/- <br /><br />*Fair usage policy apply</div>\r\n<hr />\r\n<p class="para">Fiber Broadband Service&nbsp;</p>', 0, '2017-12-26 10:34:26', '2017-12-26 07:31:45', 4);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `ID` int(11) NOT NULL,
  `Name` text NOT NULL,
  `Slug` varchar(230) NOT NULL,
  `Heading` text NOT NULL,
  `Tagline` text NOT NULL,
  `Image` text NOT NULL,
  `Text` text NOT NULL,
  `Sort` int(11) NOT NULL,
  `Status` int(11) NOT NULL,
  `PerformedBy` int(11) NOT NULL,
  `DateAdded` datetime NOT NULL,
  `DateModified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`ID`, `Name`, `Slug`, `Heading`, `Tagline`, `Image`, `Text`, `Sort`, `Status`, `PerformedBy`, `DateAdded`, `DateModified`) VALUES
(1, '', '', 'GET BLAZING FAST INTERNET', ' WITH OPTOME', '1.png', '<p>Buckle up for OptoMe fastest Internet. Unbelievable speed and boundless utilization gives you limitless conceivable outcomes - Regardless of how you utilize the Internet, OPTOME offers a super-quick, smooth, reliable connection. Get fantastic accelerates up to 99Mbps that everybody in your home can appreciate, on different gadgets, all in the meantime. Site pages stack in a snap. Gamers play at twist speed. Motion pictures stream without a hiccup.</p>', 0, 1, 4, '0000-00-00 00:00:00', '2017-12-27 14:04:44'),
(2, '', 'explore-your-imagination-', 'EXPLORE YOUR ', 'IMAGINATION!', '2.png', '<p>With our OPTOME TV Add-on service, you can experience significantly more than 100+ HD channels. Record your most loved shows and motion pictures with the world''s best DVR, get to the web''s most prevalent applications on your TV and take your excitement in a hurry! Endless enjoyment has never been easier!</p>', 1, 1, 4, '0000-00-00 00:00:00', '2017-12-27 14:05:33'),
(3, '', 'reliable-crystal-clear-voice', 'RELIABLE,', 'CRYSTAL-CLEAR VOICE', '3.png', '<p>With OptoMe phone service you can rely on, the world&rsquo;s just a call away. With our phone service, you get boundless local calling. Enjoy quality connection and clear voice, all from the comfort of your home without stressing about signal strength or dropping your call.</p>', 2, 1, 4, '0000-00-00 00:00:00', '2017-12-27 14:06:20');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE IF NOT EXISTS `services` (
  `ID` int(11) NOT NULL,
  `Heading` text NOT NULL,
  `Text` text NOT NULL,
  `Image` text NOT NULL,
  `Status` tinyint(1) NOT NULL,
  `ShowOnHome` int(11) NOT NULL,
  `Sort` int(11) NOT NULL,
  `PerformedBy` int(11) NOT NULL,
  `DateAdded` datetime NOT NULL,
  `DateModified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `ID` int(11) NOT NULL,
  `Name` text NOT NULL,
  `Website` text NOT NULL,
  `Email` text NOT NULL,
  `Phone` text NOT NULL,
  `Mobile` text NOT NULL,
  `Fax` text NOT NULL,
  `Address` text NOT NULL,
  `FooterAbout` text NOT NULL,
  `Photo` text NOT NULL,
  `DateModified` datetime NOT NULL,
  `PerformedBy` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`ID`, `Name`, `Website`, `Email`, `Phone`, `Mobile`, `Fax`, `Address`, `FooterAbout`, `Photo`, `DateModified`, `PerformedBy`) VALUES
(1, 'Â Optome', 'www.optome.com', 'sales@optome.com, support@optome.com', '111-768-662', '111-OPTOME (768-662)', 'Monday-Saturday : 9am to 6pm', '86, Block 7 & 8, J.C.H.S., Shaheed-e-Millat Road Karachi, Pakistan', '<iframe id="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3619.8298929848847!2d67.07822801420147!3d24.869658784049527!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3eb33ebac8c37189%3A0xfe116b719918f0fa!2sConnect+Communications+Pvt+Ltd.!5e0!3m2!1sen!2s!4v1511605261686" height="400" frameborder="0" style="border:0; width:100%;" allowfullscreen allowfullscreen></iframe>', '1.png', '2017-12-28 11:46:32', 4);

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE IF NOT EXISTS `sliders` (
  `ID` int(11) NOT NULL,
  `Name` text NOT NULL,
  `Heading` text NOT NULL,
  `Text` text NOT NULL,
  `Slider` text NOT NULL,
  `CornerTag` varchar(255) DEFAULT NULL,
  `RightImage` varchar(255) DEFAULT NULL,
  `URL` text NOT NULL,
  `Status` tinyint(1) NOT NULL,
  `Sort` int(11) NOT NULL,
  `PerformedBy` int(11) NOT NULL,
  `DateAdded` datetime NOT NULL,
  `DateModified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`ID`, `Name`, `Heading`, `Text`, `Slider`, `CornerTag`, `RightImage`, `URL`, `Status`, `Sort`, `PerformedBy`, `DateAdded`, `DateModified`) VALUES
(1, '', '', '', '1.jpg', NULL, NULL, '', 1, 0, 4, '2017-12-22 16:24:57', '0000-00-00 00:00:00'),
(2, '', '', '', '2.', NULL, NULL, '', 1, 0, 4, '2017-12-22 16:25:03', '2017-12-23 15:46:00'),
(3, '', '', '', '3.jpg', NULL, NULL, '', 1, 0, 4, '2017-12-22 16:25:08', '0000-00-00 00:00:00'),
(4, '', '', '', '4.jpg', NULL, NULL, '', 1, 0, 4, '2017-12-22 16:25:15', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `sociallinks`
--

CREATE TABLE IF NOT EXISTS `sociallinks` (
  `ID` int(11) NOT NULL,
  `Heading` text NOT NULL,
  `Text` text NOT NULL,
  `Image` text NOT NULL,
  `Status` tinyint(1) NOT NULL,
  `Sort` int(11) NOT NULL,
  `PerformedBy` int(11) NOT NULL,
  `DateAdded` datetime NOT NULL,
  `DateModified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sociallinks`
--

INSERT INTO `sociallinks` (`ID`, `Heading`, `Text`, `Image`, `Status`, `Sort`, `PerformedBy`, `DateAdded`, `DateModified`) VALUES
(27, 'facebook', 'facebook', '27.png', 1, 0, 4, '2017-12-22 17:02:36', '2017-12-22 17:04:11'),
(28, 'instagram', 'instagram', '28.png', 1, 0, 4, '2017-12-22 17:09:14', '0000-00-00 00:00:00'),
(29, 'twitter', 'twitter', '29.png', 1, 0, 4, '2017-12-22 17:09:32', '0000-00-00 00:00:00'),
(30, 'linkedin', 'linkedin', '30.png', 1, 0, 4, '2017-12-22 17:09:47', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `ID` int(11) NOT NULL,
  `FirstName` text NOT NULL,
  `LastName` text NOT NULL,
  `EmailAddress` text NOT NULL,
  `Password` char(32) NOT NULL,
  `Role` int(11) NOT NULL,
  `Status` tinyint(1) NOT NULL,
  `Photo` text NOT NULL,
  `DateAdded` datetime NOT NULL,
  `DateModified` datetime NOT NULL,
  `PerformedBy` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`ID`, `FirstName`, `LastName`, `EmailAddress`, `Password`, `Role`, `Status`, `Photo`, `DateAdded`, `DateModified`, `PerformedBy`) VALUES
(4, 'Admin', 'Admin', 'admin@admin.com', '202cb962ac59075b964b07152d234b70', 1, 1, '4.jpg', '2015-08-29 10:50:00', '2016-10-13 10:59:15', 4);

-- --------------------------------------------------------

--
-- Table structure for table `vacancy`
--

CREATE TABLE IF NOT EXISTS `vacancy` (
  `ID` int(11) NOT NULL,
  `Heading` text NOT NULL,
  `Text` text NOT NULL,
  `Status` int(11) NOT NULL,
  `PerformedBy` int(11) NOT NULL,
  `DateAdded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DateModified` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vacancy`
--

INSERT INTO `vacancy` (`ID`, `Heading`, `Text`, `Status`, `PerformedBy`, `DateAdded`, `DateModified`) VALUES
(1, 'SOCIAL MEDIA EXPERTS', '', 1, 4, '2017-11-24 09:40:39', '2017-11-24 00:00:00'),
(2, 'MARKETING EXECUTIVE', '', 1, 4, '2017-11-24 09:43:47', '2017-11-24 00:00:00'),
(3, 'CREATIVE CONTENT WRITER', '\r\nsssssssssssssssssssssssssssssss\r\n\r\n', 1, 4, '2017-11-24 09:44:21', '2017-11-28 15:49:29');

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE IF NOT EXISTS `video` (
  `ID` int(11) NOT NULL,
  `Thumbnail` text NOT NULL,
  `Code` text NOT NULL,
  `PerformedBy` int(11) NOT NULL,
  `DateModified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `video`
--

INSERT INTO `video` (`ID`, `Thumbnail`, `Code`, `PerformedBy`, `DateModified`) VALUES
(1, '1.jpg', '<video width="600" height="450" controls="">\n\n		  <source src="images/video.mp4" type="video/mp4">\n\n		Your browser does not support the video tag.\n\n		</video>', 4, '2016-10-14 12:18:13');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `career`
--
ALTER TABLE `career`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `careers`
--
ALTER TABLE `careers`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `deals`
--
ALTER TABLE `deals`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `downloads`
--
ALTER TABLE `downloads`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `heaingtext`
--
ALTER TABLE `heaingtext`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `home`
--
ALTER TABLE `home`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `multimedia`
--
ALTER TABLE `multimedia`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `newsletters`
--
ALTER TABLE `newsletters`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ourstory`
--
ALTER TABLE `ourstory`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `sociallinks`
--
ALTER TABLE `sociallinks`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `vacancy`
--
ALTER TABLE `vacancy`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `career`
--
ALTER TABLE `career`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `careers`
--
ALTER TABLE `careers`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `deals`
--
ALTER TABLE `deals`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `downloads`
--
ALTER TABLE `downloads`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `heaingtext`
--
ALTER TABLE `heaingtext`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `home`
--
ALTER TABLE `home`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `multimedia`
--
ALTER TABLE `multimedia`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `newsletters`
--
ALTER TABLE `newsletters`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `ourstory`
--
ALTER TABLE `ourstory`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `sociallinks`
--
ALTER TABLE `sociallinks`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `vacancy`
--
ALTER TABLE `vacancy`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
