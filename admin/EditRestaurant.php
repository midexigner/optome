<?php
include_once("Common.php");
include("CheckAdminLogin.php");


	$msg="";
	$Status=1;
	$ShowOnHome=0;
	$ID=0;
	$Heading="";
	$Text="";
	$Password="";
	$Role=1;
	$Image="";
	foreach($_REQUEST as $key => $val)
		$$key = $val;
		
if(isset($_POST["action"]) && $_POST["action"] == "submit_form")
{


	if($Heading == "")
	{
		$msg='<div class="alert alert-danger alert-dismissable">
		<i class="fa fa-ban"></i>
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<b>Please enter Heading.</b>
		</div>';
	}
	else if($Text == "")
	{
		$msg='<div class="alert alert-danger alert-dismissable">
		<i class="fa fa-ban"></i>
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<b>Please enter Text.</b>
		</div>';
	}

	if($msg=="")
	{
		$query="UPDATE restaurants SET DateAdded=NOW(),
				Heading = '" . dbinput($Heading) . "',
				Text = '" . dbinput($Text) . "',
				Status='".(int)$Status . "',
				ShowOnHome='".(int)$ShowOnHome . "',
				PerformedBy = '" . dbinput($_SESSION['UserID']) . "'
				WHERE ID='".(int)$ID."'
				";
		mysql_query($query) or die (mysql_error());
		// echo $query;
		$_SESSION["msg"]='<div class="alert alert-success alert-dismissable">
		<i class="fa fa-ban"></i>
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<b>Restaurant has been updated.</b>
		</div>';		
		
		if(isset($_FILES["flPage"]) && $_FILES["flPage"]['name'] != "")
		{
			$num=count($_FILES["flPage"]['name']);
			$Dbinputarray = "";
			$success = 0;
			for($i = 0 ; $i < $num ; $i++)
			{
				$filenamearray=explode(".", $_FILES["flPage"]['name'][$i]);
				$ext=strtolower($filenamearray[sizeof($filenamearray)-1]);
				$tempName = $_FILES["flPage"]['tmp_name'][$i];
				$realName = $ID . "-".$i."." . $ext;
				$StoreImage = $realName; 
				$target = DIR_RESTAURANTS . $realName;
				$Dbinputarray .= $realName.",";
				$moved = move_uploaded_file($tempName, $target);
				if($moved)
					$success++;
			}
			$Dbinputarray = substr($Dbinputarray,0,strlen($Dbinputarray)-1);
			if($success==$num)
			{			
			
				$query="UPDATE restaurants SET Image='" . dbinput($Dbinputarray) . "' WHERE  ID=" . (int)$ID;
				mysql_query($query) or die(mysql_error());
			}
			else
			{
				$_SESSION["msg"]='<div class="alert alert-warning alert-dismissable">
					<i class="fa fa-ban"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<b>Restaurant has been saved but Image can not be uploaded.</b>
					</div>';
			}
		}
		
		redirect("Restaurants.php");	
	}
		

}
else
{
	$query="SELECT * FROM restaurants WHERE  ID='" . (int)$ID . "'";
	
	$result = mysql_query ($query) or die(mysql_error()); 
	$num = mysql_num_rows($result);
	
	if($num==0)
	{
		$_SESSION["msg"]='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Invalid Restaurant ID.</b>
		</div>';
		redirect("Restaurants.php");
	}
	else
	{
		$row = mysql_fetch_array($result,MYSQL_ASSOC);
		foreach($row as $key=> $val)
			$$key = $val;
		$Img = explode(',',$Image);
		$numb=count($Img);
	}
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Edit Restaurant</title>

    <!-- Bootstrap -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Dropzone.js -->
    <link href="vendors/dropzone/dist/min/dropzone.min.css" rel="stylesheet">
    
    <!-- Custom Theme Style -->
    <link href="build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <?php include_once("Sidebar.php"); ?>

        <?php include_once("Header.php"); ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Edit Restaurant</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                              <button class="btn btn-default" type="button">Go!</button>
                          </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Insert Form</h2>
                    <ul class="nav navbar-right panel_toolbox">
					  <li><a href="Restaurants.php" class="btn btn-default active"><i class="fa fa-arrow-left"></i> Back</a></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
				  <?php
		  		echo $msg;
				if(isset($_SESSION["msg"]))
				{
					echo $_SESSION["msg"];
					$_SESSION["msg"]="";
				}
				?>
                  <div class="x_content">

                    <form id="frmPages"  method="post" action="<?php echo $self;?>?ID=<?php echo $ID; ?>" enctype="multipart/form-data" class="form-horizontal form-label-left" novalidate>
					  <input type="hidden" id="action" name="action" value="submit_form" />
                      <span class="section">Fill All Mandatory Fields</span>

					  <div class="item form-group">
						  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Image">Image <span class="required">*</span></label>
						  <div class="col-md-6 col-sm-6 col-xs-12">
                  <div id="images2">
				  <?php
				  for($i = 0 ; $i < $numb ; $i++)
				  {
				  //$Img=explode(",", $Image[$i]);
					if($numb > 0 && $Img[0] !="")
					{
					echo '<img src="'. DIR_RESTAURANTS . $Img[$i] . '" width="110" height="100">
						<input type="hidden" name="flPage[]" value="'.$Img[$i].'" />
						';
					?>
					<img id="img" src="images/x.png" onClick="location.href='DeleteImage.php?Name=<?php echo $Img[$i]; ?>&ProdID=<?php echo $ID; ?>'" alt="delete" class="cursor-pointer">
					<?php }
				  }
					?><br><br>
				  </div>
                    <div id="filediv" style="display: inline-block"><input name="flPage[]" class="form-control col-md-7 col-xs-12" type="file" id="flPage"/></div>
                    <input type="button" id="add_more" class="btn btn-default" value="Add More Files" style="display:block"/>
						  <p class="help-block">Image types allowed: jpg, jpeg, gif, png. --- Image size must be <?php echo MAX_IMAGE_SIZE/1024 ?> MB or less.</p>
						  </div>
						</div>
					  
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Heading">Heading <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="Heading" class="form-control col-md-7 col-xs-12" data-validate-length-range="3" name="Heading" required="required" type="text" value="<?php echo $Heading; ?>">
                        </div>
                      </div>
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Text">Text <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea id="Text" class="form-control col-md-7 col-xs-12" data-validate-length-range="3" name="Text" required="required" ><?php echo $Text; ?></textarea>
                        </div>
                      </div>
					  <div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
						<div class="col-md-6 col-sm-6 col-xs-12" style="margin-top:8px;">
							<label>
							  <input type="radio" name="Status" value="1" <?php echo ($Status == '1' ? 'checked' : ''); ?>> &nbsp; Active &nbsp;
							</label>
							<label>
							  <input type="radio" name="Status" value="0" <?php echo ($Status == '0' ? 'checked' : ''); ?>> &nbsp; Deactive &nbsp;
							</label>
						</div>
					  </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <a href="Restaurants.php" class="btn btn-primary">Cancel</a>
                          <button id="send" type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <?php include_once("Footer.php"); ?>
      </div>
    </div>

    <!-- jQuery -->
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="vendors/nprogress/nprogress.js"></script>
    <!-- validator -->
    <script src="vendors/validator/validator.min.js"></script>
    <!-- Dropzone.js -->
    <script src="vendors/dropzone/dist/min/dropzone.min.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="build/js/custom.min.js"></script>

    <!-- validator -->
    <script>
	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#imgpreview')
                    .attr('src', e.target.result)
                    .height(142);
            };
            reader.readAsDataURL(input.files[0]);
        }
		else
                $('#imgpreview').attr('src', '').width(0).height(0);
    }
      // initialize the validator function
      validator.message.date = 'not a real date';

      // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
      $('form')
        .on('blur', 'input[required], input.optional, select.required', validator.checkField)
        .on('change', 'select.required', validator.checkField)
        .on('keypress', 'input[required][pattern]', validator.keypress);

      $('.multi.required').on('keyup blur', 'input', function() {
        validator.checkField.apply($(this).siblings().last()[0]);
      });

      $('form').submit(function(e) {
        e.preventDefault();
        var submit = true;

        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
          submit = false;
        }

        if (submit)
          this.submit();

        return false;
      });
    </script>
<script>
var abc = 0; //Declaring and defining global increement variable

$(document).ready(function() {

//To add new input file field dynamically, on click of "Add More Files" button below function will be executed
    $('#add_more').click(function() {
        $(this).before($("<div/>", {id: 'filediv', style: 'display: inline-block'}).fadeIn('slow').append(
                $("<input/>", {name: 'flPage[]', type: 'file', id: 'flPage', class: 'form-control col-md-7 col-xs-12'}),        
                $("<br/>")
                ));
    });

//following function will executes on change event of file input to select different file	
$('body').on('change', '#flPage', function(){
            if (this.files && this.files[0]) {
                 abc += 1; //increementing global variable by 1
				
				var z = abc - 1;
                var x = $(this).parent().find('#previewimg' + z).remove();
                $(this).before("<div id='abcd"+ abc +"' class='abcd' style='display: inline'><img id='previewimg" + abc + "' src='' style='max-height: 100px; max-width: 100px'/></div>");
               
			    var reader = new FileReader();
                reader.onload = imageIsLoaded;
                reader.readAsDataURL(this.files[0]);
               
			    $(this).hide();
                $("#abcd"+ abc).append($("<img/>", {id: 'img', src: 'images/x.png', alt: 'delete', class: 'cursor-pointer'}).click(function() {
                $(this).parent().parent().remove();
                }));
            }
        });

//To preview image     
    function imageIsLoaded(e) {
        $('#previewimg' + abc).attr('src', e.target.result);
    };

    $('#upload').click(function(e) {
        var name = $(":file").val();
        if (!name)
        {
            alert("First Image Must Be Selected");
            e.preventDefault();
        }
    });
});
</script>
    <!-- /validator -->
  </body>
</html>