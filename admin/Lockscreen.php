<?php include_once("Common.php"); ?>
<?php
if($_SESSION["Login"]==false || $_SESSION["LockScreen"]==false)
{
	redirect("Dashboard.php");	
}
else
{
	$pswd = "";
	$msg = "";
	if(isset($_POST["action"]) && $_POST["action"] == "submit_form")
	{
		if(isset($_POST["pswd"]))
			$pswd=trim($_POST["pswd"]);
			
		if ($pswd=="")
			$msg = "<div class=\"error\">&emsp;Please Enter Password.</div>";
			
		if($msg=='')
		{	
			if($_SESSION["Login"]==true)
			{
				$query="SELECT Password FROM users WHERE Status = 1 AND Role='".$_SESSION["RoleID"]."' AND ID='".$_SESSION["UserID"]."'";
				$result = mysql_query ($query) or die(mysql_error()); 
				$num = mysql_num_rows($result);
				mysql_close($dbh);
				
				if($num==1)
				{
					$row = mysql_fetch_array($result,MYSQL_ASSOC);
					if(dboutput($row["Password"]) == md5($pswd))
					{
						$_SESSION["LockScreen"]=false;
						redirect("Dashboard.php");
					}
				}
				
				$msg = "<div class=\"error\">Invalid Password.</div>";
				
			}
			else
			{
				redirect("Logout.php");	
			}
		}
	}
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Lock Screen</title>

    <!-- Bootstrap -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="https://colorlib.com/polygon/gentelella/css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>
		
      <div class="login_wrapper">
	  
        <div class="animate form login_form">
		<?php 
					if(isset($msg3))
					echo $msg3
					?>
          <section class="login_content">
            <form action="<?php echo $_SERVER["PHP_SELF"];?>" method="post">
              <h1>Unlock Form</h1>
              <div>
				<span style="color:red;font-size:12px;"><?php if(isset($msg)){echo $msg;}?></span>
                <input type="password" name="pswd" class="form-control" placeholder="Password" value="" />
              </div>
              <div>
				<input type="hidden" name="action" value="submit_form" />
				<button type="submit" class="btn btn-default">Unlock Now</button>
				<label><a href="Logout.php">Or sign in as a different user</a></label>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                

                <div>
                  <h1><img src="<?php echo (is_file(DIR_SETTINGS . $SETTING_Photo) ? DIR_SETTINGS . $SETTING_Photo : 'images/avatar2.png'); ?>" style="height:50px;width:50px;" /> <?php echo $SETTING_Name; ?></h1>
                  <p style="margin-top:-20px;">Copyright &copy; <?php echo date('Y'); ?> - All Rights Reserved</p>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
  </body>
</html>