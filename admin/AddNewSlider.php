<?php
include_once("Common.php");
include("CheckAdminLogin.php");


	$msg="";
	$Status=1;
	$ID=0;
	$Heading="";
	$Text="";
	$Password="";
	$Role=1;
	$Slider="";
	$CornerTag = "";
	$RightImage = "";
		
if(isset($_POST["action"]) && $_POST["action"] == "submit_form")
{			
	if(isset($_POST["Status"]) && ((int)$_POST["Status"] == 0 || (int)$_POST["Status"] == 1))
		$Status=trim($_POST["Status"]);	
	// if(isset($_POST["Role"]) && ((int)$_POST["Role"] == 1 || (int)$_POST["Role"] == 2))
		// $Role=trim($_POST["Role"]);	
	// if(isset($_POST["Heading"]))
	// 	$Heading=trim($_POST["Heading"]);
	// if(isset($_POST["Text"]))
	// 	$Text=trim($_POST["Text"]);
	if(isset($_POST["flPage"]) && $_FILES["flPage"]['name'] != "")
	{
		$filenamearray=explode(".", $_FILES["flPage"]['name']);
		$ext=strtolower($filenamearray[sizeof($filenamearray)-1]);
	
		if(!in_array($ext, $_IMAGE_ALLOWED_TYPES))
		{
			$msg='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Only '.implode(", ", $_IMAGE_ALLOWED_TYPES) . ' files can be uploaded.</b>
			</div>';
		}			
		else if($_FILES["flPage"]['size'] > (MAX_IMAGE_SIZE*1024))
		{
			$msg='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Image size must be ' . MAX_IMAGE_SIZE . ' KB or less.</b>
			</div>';
		}
	}
	// if(isset($_POST["flPage1"]) && $_FILES["flPage1"]['name'] != "")
	// {
	// 	$filenamearray=explode(".", $_FILES["flPage1"]['name']);
	// 	$ext=strtolower($filenamearray[sizeof($filenamearray)-1]);
	
	// 	if(!in_array($ext, $_IMAGE_ALLOWED_TYPES))
	// 	{
	// 		$msg='<div class="alert alert-danger alert-dismissable">
	// 		<i class="fa fa-ban"></i>
	// 		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	// 		<b>Only '.implode(", ", $_IMAGE_ALLOWED_TYPES) . ' files can be uploaded.</b>
	// 		</div>';
	// 	}			
	// 	else if($_FILES["flPage1"]['size'] > (MAX_IMAGE_SIZE*1024))
	// 	{
	// 		$msg='<div class="alert alert-danger alert-dismissable">
	// 		<i class="fa fa-ban"></i>
	// 		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	// 		<b>Image size must be ' . MAX_IMAGE_SIZE . ' KB or less.</b>
	// 		</div>';
	// 	}
	// }
	// if(isset($_POST["flPage2"]) && $_FILES["flPage2"]['name'] != "")
	// {
	// 	$filenamearray=explode(".", $_FILES["flPage2"]['name']);
	// 	$ext=strtolower($filenamearray[sizeof($filenamearray)-1]);
	
	// 	if(!in_array($ext, $_IMAGE_ALLOWED_TYPES))
	// 	{
	// 		$msg='<div class="alert alert-danger alert-dismissable">
	// 		<i class="fa fa-ban"></i>
	// 		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	// 		<b>Only '.implode(", ", $_IMAGE_ALLOWED_TYPES) . ' files can be uploaded.</b>
	// 		</div>';
	// 	}			
	// 	else if($_FILES["flPage2"]['size'] > (MAX_IMAGE_SIZE*1024))
	// 	{
	// 		$msg='<div class="alert alert-danger alert-dismissable">
	// 		<i class="fa fa-ban"></i>
	// 		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	// 		<b>Image size must be ' . MAX_IMAGE_SIZE . ' KB or less.</b>
	// 		</div>';
	// 	}
	// }

	// 	if($Heading == "")
	// 	{
	// 		$msg='<div class="alert alert-danger alert-dismissable">
	// 		<i class="fa fa-ban"></i>
	// 		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	// 		<b>Please enter Caption Heading.</b>
	// 		</div>';
	// 	}
	// 	else if($Text == "")
	// 	{
	// 		$msg='<div class="alert alert-danger alert-dismissable">
	// 		<i class="fa fa-ban"></i>
	// 		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	// 		<b>Please enter Caption Para.</b>
	// 		</div>';
	// 	}

	if($msg=="")
	{
		$query="INSERT INTO sliders SET DateAdded=NOW(),
				Heading = '" . dbinput($Heading) . "',
				Text = '" . dbinput($Text) . "',
				Status='".(int)$Status . "',
				PerformedBy = '" . dbinput($_SESSION['UserID']) . "'";
		mysql_query($query) or die (mysql_error());
		// echo $query;
		$ID = mysql_insert_id();
		$_SESSION["msg"]='<div class="alert alert-success alert-dismissable">
		<i class="fa fa-ban"></i>
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<b>Slider has been added.</b>
		</div>';		
		
		if(isset($_FILES["flPage"]) && $_FILES["flPage"]['name'] != "")
		{
			
		
			$filenamearray=explode(".", $_FILES["flPage"]['name']);
			$ext=strtolower($filenamearray[sizeof($filenamearray)-1]);
			$tempName = $_FILES["flPage"]['tmp_name'];
			$realName = $ID . "." . $ext;
			$StoreImage = $realName; 
			$target = DIR_SLIDERS . $realName;
			if(is_file(DIR_SLIDERS . $StoreImage))
				unlink(DIR_SLIDERS . $StoreImage);
			$moved=move_uploaded_file($tempName, $target);
		
			if($moved)
			{			
			
				$query="UPDATE sliders SET Slider='" . dbinput($realName) . "' WHERE  ID=" . (int)$ID;
				mysql_query($query) or die(mysql_error());
			}
			else
			{
				$_SESSION["msg"]='<div class="alert alert-warning alert-dismissable">
					<i class="fa fa-ban"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<b>Slider has been saved but Slider can not be uploaded.</b>
					</div>';
			}
		}

		if(isset($_FILES["flPage1"]) && $_FILES["flPage1"]['name'] != "")
		{
			
		
			$filenamearray1=explode(".", $_FILES["flPage1"]['name']);
			$ext1=strtolower($filenamearray1[sizeof($filenamearray1)-1]);
			$tempName1 = $_FILES["flPage1"]['tmp_name'];
			$realName1 = $ID . "_1." . $ext1;
			$StoreImage1 = $realName1; 
			$target1 = DIR_SLIDERS . $realName1;
			if(is_file(DIR_SLIDERS . $StoreImage1))
				unlink(DIR_SLIDERS . $StoreImage1);
			$moved1=move_uploaded_file($tempName1, $target1);
		
			if($moved1)
			{			
			
				$query1="UPDATE sliders SET CornerTag='" . dbinput($realName1) . "' WHERE  ID=" . (int)$ID;
				mysql_query($query1) or die(mysql_error());
			}
			else
			{
				$_SESSION["msg"]='<div class="alert alert-warning alert-dismissable">
					<i class="fa fa-ban"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<b>Slider has been saved but Slider can not be uploaded.</b>
					</div>';
			}
		}

		if(isset($_FILES["flPage2"]) && $_FILES["flPage2"]['name'] != "")
		{
			
		
			$filenamearray2=explode(".", $_FILES["flPage2"]['name']);
			$ext2=strtolower($filenamearray2[sizeof($filenamearray2)-1]);
			$tempName2 = $_FILES["flPage2"]['tmp_name'];
			$realName2 = $ID . "_2." . $ext2;
			$StoreImage2 = $realName2; 
			$target2 = DIR_SLIDERS . $realName2;
			if(is_file(DIR_SLIDERS . $StoreImage2))
				unlink(DIR_SLIDERS . $StoreImage2);
			$moved2=move_uploaded_file($tempName2, $target2);
		
			if($moved2)
			{			
			
				$query2="UPDATE sliders SET RightImage='" . dbinput($realName2) . "' WHERE  ID=" . (int)$ID;
				mysql_query($query2) or die(mysql_error());
			}
			else
			{
				$_SESSION["msg"]='<div class="alert alert-warning alert-dismissable">
					<i class="fa fa-ban"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<b>Slider has been saved but Slider can not be uploaded.</b>
					</div>';
			}
		}
		
		redirect("AddNewSlider.php");	
	}
		

}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Add Slider</title>

    <!-- Bootstrap -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    
    <!-- Custom Theme Style -->
    <link href="build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <?php include_once("Sidebar.php"); ?>

        <?php include_once("Header.php"); ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Add Slider</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                              <button class="btn btn-default" type="button">Go!</button>
                          </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Insert Form</h2>
                    <ul class="nav navbar-right panel_toolbox">
					  <li><a href="Sliders.php" class="btn btn-default active"><i class="fa fa-arrow-left"></i> Back</a></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
				  <?php
		  		echo $msg;
				if(isset($_SESSION["msg"]))
				{
					echo $_SESSION["msg"];
					$_SESSION["msg"]="";
				}
				?>
                  <div class="x_content">

                    <form id="frmPages"  method="post" action="<?php echo $self;?>" enctype="multipart/form-data" class="form-horizontal form-label-left" novalidate>
					  <input type="hidden" id="action" name="action" value="submit_form" />
                      <span class="section">Fill All Mandatory Fields</span>

					  <div class="item form-group">
						  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Slider">Slider <span class="required">*</span></label>
						  <div class="col-md-6 col-sm-6 col-xs-12">
						  <img id="imgpreview" src="" />
						  <input type="file" name="flPage" class="form-control col-md-7 col-xs-12" onchange="readURL(this);" required="required" />
						  <p class="help-block">Image types allowed: jpg, jpeg, gif, png. --- Image size must be <?php echo MAX_IMAGE_SIZE/1024 ?> MB or less.</p>
						  </div>
						</div>

						<!-- <div class="item form-group">
						  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Slider">Corner Tag <span class="required"></span></label>
						  <div class="col-md-6 col-sm-6 col-xs-12">
						  <img id="imgpreview1" src="" />
						  <input type="file" name="flPage1" class="form-control col-md-7 col-xs-12" onchange="readURL1(this);" />
						  <p class="help-block">Image types allowed: jpg, jpeg, gif, png. --- Image size must be <?php echo MAX_IMAGE_SIZE/1024 ?> MB or less.</p>
						  </div>
						</div>

						<div class="item form-group">
						  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Slider">Slide Image<span class="required"></span></label>
						  <div class="col-md-6 col-sm-6 col-xs-12">
						  <img id="imgpreview2" src="" />
						  <input type="file" name="flPage2" class="form-control col-md-7 col-xs-12" onchange="readURL2(this);" />
						  <p class="help-block">Image types allowed: jpg, jpeg, gif, png. --- Image size must be <?php echo MAX_IMAGE_SIZE/1024 ?> MB or less.</p>
						  </div>
						</div>
					  
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Heading">Caption Heading <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="Heading" class="form-control col-md-7 col-xs-12" data-validate-length-range="3" name="Heading" required="required" type="text" value="<?php echo $Heading; ?>">
                        </div>
                      </div>
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Text">Caption Para <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea id="Text" class="form-control col-md-7 col-xs-12" data-validate-length-range="3" name="Text" required="required" ><?php echo $Text; ?></textarea>
                        </div>
                      </div> -->
					  <div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
						<div class="col-md-6 col-sm-6 col-xs-12" style="margin-top:8px;">
							<label>
							  <input type="radio" name="Status" value="1" <?php echo ($Status == '1' ? 'checked' : ''); ?>> &nbsp; Active &nbsp;
							</label>
							<label>
							  <input type="radio" name="Status" value="0" <?php echo ($Status == '0' ? 'checked' : ''); ?>> &nbsp; Deactive &nbsp;
							</label>
						</div>
					  </div>
					  
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <a href="Sliders.php" class="btn btn-primary">Cancel</a>
                          <button id="send" type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <?php include_once("Footer.php"); ?>
      </div>
    </div>

    <!-- jQuery -->
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="vendors/nprogress/nprogress.js"></script>
    <!-- validator -->
    <script src="vendors/validator/validator.min.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="build/js/custom.min.js"></script>

    <!-- validator -->
    <script>
		function readURL(input) {
	        if (input.files && input.files[0]) {
	            var reader = new FileReader();
	            reader.onload = function (e) {
	                $('#imgpreview')
	                    .attr('src', e.target.result)
	                    .width(400)
	                    .height(142);
	            };
	            reader.readAsDataURL(input.files[0]);
	        }
			else
	                $('#imgpreview').attr('src', '').width(0).height(0);
	    }
	    function readURL1(input) {
	        if (input.files && input.files[0]) {
	            var reader = new FileReader();
	            reader.onload = function (e) {
	                $('#imgpreview1')
	                    .attr('src', e.target.result)
	                    .width(400)
	                    .height(142);
	            };
	            reader.readAsDataURL(input.files[0]);
	        }
			else
	                $('#imgpreview1').attr('src', '').width(0).height(0);
	    }
	    function readURL2(input) {
	        if (input.files && input.files[0]) {
	            var reader = new FileReader();
	            reader.onload = function (e) {
	                $('#imgpreview2')
	                    .attr('src', e.target.result)
	                    .width(400)
	                    .height(142);
	            };
	            reader.readAsDataURL(input.files[0]);
	        }
			else
	                $('#imgpreview2').attr('src', '').width(0).height(0);
	    }
      // initialize the validator function
      validator.message.date = 'not a real date';

      // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
      $('form')
        .on('blur', 'input[required], input.optional, select.required', validator.checkField)
        .on('change', 'select.required', validator.checkField)
        .on('keypress', 'input[required][pattern]', validator.keypress);

      $('.multi.required').on('keyup blur', 'input', function() {
        validator.checkField.apply($(this).siblings().last()[0]);
      });

      $('form').submit(function(e) {
        e.preventDefault();
        var submit = true;

        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
          submit = false;
        }

        if (submit)
          this.submit();

        return false;
      });
    </script>
    <!-- /validator -->
  </body>
</html>