<?php include("Common.php"); ?>
<?php include("CheckAdminLogin.php"); ?>
<?php 
	$action = "";
	$msg = "";
	if(isset($_POST["action"]))
		$action = $_POST["action"];
if($action == "delete")
{
	if(isset($_REQUEST["ids"]) && is_array($_REQUEST["ids"]))
	{
		foreach($_REQUEST["ids"] as $BID)
		{
			$row = mysql_query("SELECT Slider FROM clients  WHERE ID = ". $BID ."") or die (mysql_error());
			$dt = mysql_fetch_array($row);
			if(is_file(DIR_SLIDERS . $dt['Slider']))
				unlink(DIR_SLIDERS . $dt['Slider']);
			mysql_query("DELETE FROM clients  WHERE ID IN (" . $BID . ")") or die (mysql_error());
		}
		$_SESSION["msg"]='<div class="alert alert-success alert-dismissable">
		<i class="fa fa-ban"></i>
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<b>Delete All selected sliders.</b>
		</div>';
		redirect("Sliders.php");
	
	}
	else
	{
		$msg='<div class="alert alert-danger alert-dismissable">
		<i class="fa fa-ban"></i>
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<b>Please select slider to delete.</b>
		</div>';
	}
}
if(isset($_REQUEST['SID']))
{
	$query = "UPDATE clients SET Status = IF(Status=1, 0, 1) WHERE ID = ".$_REQUEST['SID']."";
	mysql_query($query);
	$_SESSION["msg"] = '<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<i class="icon fa fa-check"></i> Slider status changed!
		  </div>';
	redirect($_SERVER["PHP_SELF"]);
}
if(isset($_REQUEST['DID']))
{
	$row = mysql_query("SELECT Slider FROM clients  WHERE ID = ". $_REQUEST["DID"] ."") or die (mysql_error());
	$dt = mysql_fetch_array($row);
	if(is_file(DIR_SLIDERS . $dt['Slider']))
		unlink(DIR_SLIDERS . $dt['Slider']);
	$query = "DELETE FROM clients WHERE ID = ".$_REQUEST['DID']."";
	mysql_query($query);
	$_SESSION["msg"] = '<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<i class="icon fa fa-ban"></i> Slider Deleted!
		  </div>';
	redirect($_SERVER["PHP_SELF"]);
}
else if($action == 'update')
{
	$i = 0;
	foreach($_POST['orderid'] as $CID)	
	{
		
		$query = "Update clients Set `Sort` = '".$_POST['order'][$i]."' WHERE ID = ".$CID."";
		//echo $query;
		//echo '<br>';

		mysql_query($query) or die(mysql_error());
		$i++;
		
	}
			//exit();

}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Clients</title>

	<!-- Bootstrap -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendors/bootstrap/dist/css/bootstrap-toggle.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="build/css/custom.min.css" rel="stylesheet">
	
	
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        
		<?php include_once("Sidebar.php"); ?>
		
		
		<?php include_once("Header.php"); ?>

       
		<?php 
		$query="SELECT ID,Heading,Slider,Sort,Status,DATE_FORMAT(DateAdded, '%D %b %Y<br>%r') AS Added, 
				DATE_FORMAT(DateModified, '%D %b %Y<br>%r') AS Updated
				FROM clients WHERE ID <>0 ORDER BY Sort ASC";
		$result = mysql_query ($query) or die(mysql_error()); 
		$num = mysql_num_rows($result);
		?>
		
        <!-- page content -->
        <div class="right_col" role="main" style="min-height:0px; !important">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Clients <small>Prefereable size: (1400 x 500)px</small></h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
             
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Clients</h2>
					
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a href="AddNewClients.php" class="btn btn-success active"><i class="fa fa-plus-square"></i> Add New</a></li>
					  <li><a onClick="javascript:doDelete()" class="btn btn-default active"><i class="fa fa-trash"></i> Bulk Delete</a></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
				   <?php
						echo $msg;
						if(isset($_SESSION["msg"]) && $_SESSION["msg"]!="")
						{
							echo $_SESSION["msg"];
							$_SESSION["msg"]="";
						}
					?>
                  <div class="x_content">
					<form id="frmPages" action="<?php echo $self;?>" class="form-horizontal no-margin" method="post">
					<input type="hidden" id="action" name="action" value="" />
					<input type="hidden" id="DeleteID" name="DeleteID" value="" />
                    <table class="table table-bordered table-striped sorted_table">
					  <thead>
					  <tr>
                        <th><input type="checkbox" name="chkAll" class="checkUncheckAll"></td>
                        <th>Slider</th>
                       <th>Status</th>
                        <th>Added</th>
                        <th>Updated</th>
                        <th></th>
					  </tr>
					  </thead>
					  <tbody>
<?php while($row=mysql_fetch_array($result))
{
	?>
                      <tr>
                        <td>
						<input type="hidden" name="item[]" value="<?php echo $row["ID"]; ?>" />
						<input type="checkbox" value="<?php echo $row["ID"]; ?>" name="ids[]" class="no-margin chkIds"></td>
                        <td><?php echo (is_file(DIR_SLIDERS . $row['Slider']) ? '<img src="'.DIR_SLIDERS.dboutput($row['Slider']).'"  style="width:246px;height:120px" />' : ''); ?></td>
                       
                        <td>
						  <label onclick="toggleStatus(<?php echo $row["ID"]; ?>)">
							<input type="checkbox" <?php echo ($row["Status"] == 1 ? 'checked' : ''); ?> data-toggle="toggle" data-onstyle="success" data-offstyle="danger" data-size="mini" class="togglebutton" data-on="Enable" data-off="Disable">
						  </label>
						</td>
                        <td><?php echo $row["Added"]; ?></td>
                        <td><?php echo $row["Updated"]; ?></td>
						<td align="center" class="noPrint">
							<button class="btn bg-navy btn-xs" type="button" onClick="window.location.href='EditClients.php?ID=<?php echo $row["ID"]; ?>'"><i class="fa fa-pencil"></i></button>
							<button class="btn bg-navy btn-xs" type="button" onClick="javascript:SingleDelete('<?php echo $row["ID"]; ?>')"><i class="fa fa-trash"></i></button>
						</td>
					  </tr>
<?php }
	?>
					  </tbody>
					</table>
					</form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

		<?php include_once("Footer.php"); ?>
        
      </div>
    </div>

    <!-- jQuery -->
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="vendors/bootstrap/dist/js/bootstrap-toggle.min.js"></script>
    <!-- FastClick -->
    <script src="vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="vendors/nprogress/nprogress.js"></script>
    <!-- Datatables -->
    <script src="vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="vendors/datatables.net/js/jquery-sortable.js"></script>
    <script src="vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script src="vendors/jszip/dist/jszip.min.js"></script>
    <script src="vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="build/js/custom.min.js"></script>
	<style>
body.dragging, body.dragging * {
  cursor: move !important;
}

.dragged {
  position: absolute;
  opacity: 0.5;
  z-index: 2000;
}

ol.example li.placeholder {
  position: relative;
  /** More li styles **/
}
ol.example li.placeholder:before {
  position: absolute;
  /** Define arrowhead **/
}
</style>

    <!-- Datatables -->
    <script>
       $(document).ready(function() {
// $('.sorted_table').sortable({
//   containerSelector: 'table',
//   itemPath: '> tbody',
//   itemSelector: 'tr',
//   placeholder: '<tr class="placeholder"/>',
//   draggedClass: 'none',
//         onDrop: function (event, ui) {
// 	        var data = $('#frmPages').serialize();
// 			// alert('asd');
// //            $('span').text(data);
//             $.ajax({
//                     data: data,
//                 // type: 'GET',
//                 type: 'POST',
//                 url: 'Sliders2.php',
// 				success: function(odata) {
// 					// alert(odata);
// 					},
// 					error: function(xhr, textStatus, errorThrown) {
// 						alert(xhr.responseText);
// 					}
//             });
// 	}

// });
    $('ol.example').sortable({
        axis: 'y',
    });
		$('#datatable-responsive').DataTable({
				"aoColumnDefs": [ { "bSortable": false, "aTargets": [ 0, 3] } ],
				"iDisplayLength": 10,
				"aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]]
			});
      });
    </script>
    <!-- /Datatables -->
	
	<script language="javascript">
	$(document).ready(function () {				
		$(".checkUncheckAll").click(function () {
			$(".chkIds").prop("checked", $(this).prop("checked"));			
		});
	});
	var counter = 0;
	
	
	function doDelete()
	{
		if($(".chkIds").is(":checked"))
		{
			if(confirm("Are you sure to delete."))
			{
				$("#action").val("delete");
				$("#frmPages").submit();
			}
		}
		else
			alert("Please select User to delete");
	}
	
  $(function() {
    $('.togglebutton').bootstrapToggle({
      on: 'Yes',
      off: 'No'
    });
  })
  function SingleDelete(did) { if(confirm("Are you sure to delete.")) { window.location.href='<?php echo $self;?>?DID='+did; } }
  function toggleStatus(sid) { $.ajax({ url: '<?php echo $self;?>?SID='+sid }); }
	
</script>

  </body>
</html>