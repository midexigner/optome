-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 03, 2016 at 02:29 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ramada_old`
--

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `ID` int(11) NOT NULL,
  `Name` text NOT NULL,
  `EmailAddress` text NOT NULL,
  `Country` text NOT NULL,
  `Phone` text NOT NULL,
  `Message` text NOT NULL,
  `Overview` text NOT NULL,
  `Status` tinyint(1) NOT NULL,
  `DateAdded` datetime NOT NULL,
  `DateModified` datetime NOT NULL,
  `PerformedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`ID`, `Name`, `EmailAddress`, `Country`, `Phone`, `Message`, `Overview`, `Status`, `DateAdded`, `DateModified`, `PerformedBy`) VALUES
(1, 'Zeeshan', 'Zeeshan@gmail.com', 'PAK', '+923451234567', 'test', 'tests', 1, '2016-06-14 00:00:00', '2016-06-14 12:01:03', 16);

-- --------------------------------------------------------

--
-- Table structure for table `leads`
--

CREATE TABLE `leads` (
  `ID` int(11) NOT NULL,
  `Arrival` datetime NOT NULL,
  `Departure` datetime NOT NULL,
  `Adults` int(11) NOT NULL DEFAULT '0',
  `Child` int(11) NOT NULL DEFAULT '0',
  `Rooms` int(11) NOT NULL DEFAULT '0',
  `RoomType` text NOT NULL,
  `PaymentMethod` text NOT NULL,
  `PickDropAirport` text NOT NULL,
  `Name` text NOT NULL,
  `Contact` text NOT NULL,
  `Passport` text NOT NULL,
  `Address` text NOT NULL,
  `CP` text NOT NULL,
  `BkrName` text NOT NULL,
  `BkrContact` text NOT NULL,
  `Overview` text NOT NULL,
  `Status` tinyint(1) NOT NULL,
  `DateAdded` datetime NOT NULL,
  `DateModified` datetime NOT NULL,
  `PerformedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `leads`
--

INSERT INTO `leads` (`ID`, `Arrival`, `Departure`, `Adults`, `Child`, `Rooms`, `RoomType`, `PaymentMethod`, `PickDropAirport`, `Name`, `Contact`, `Passport`, `Address`, `CP`, `BkrName`, `BkrContact`, `Overview`, `Status`, `DateAdded`, `DateModified`, `PerformedBy`) VALUES
(3, '2016-07-13 00:00:00', '2016-07-15 00:00:00', 3, 2, 2, 'PM', 'Cash', 'Yes', 'Test Guest', '+923451234567', '0011-0012-0013-0014', 'Dummy Address', 'Company', 'Test Booker', '+923451234567', '', 0, '2016-07-13 00:00:00', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pg_about`
--

CREATE TABLE `pg_about` (
  `ID` int(11) NOT NULL,
  `MainHeading` text NOT NULL,
  `MainSlogan` text NOT NULL,
  `MainPara` text NOT NULL,
  `Banner` text NOT NULL,
  `Status` int(11) NOT NULL,
  `PerformedBy` int(11) NOT NULL,
  `DateAdded` datetime NOT NULL,
  `DateModified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `ID` int(11) NOT NULL,
  `Name` text NOT NULL,
  `OrderCode` text NOT NULL,
  `Server` text NOT NULL,
  `Username` text NOT NULL,
  `Password` char(32) NOT NULL,
  `DB` text NOT NULL,
  `Currency` text NOT NULL,
  `SymbolAppear` tinyint(1) NOT NULL,
  `Photo` text NOT NULL,
  `DateModified` datetime NOT NULL,
  `PerformedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`ID`, `Name`, `OrderCode`, `Server`, `Username`, `Password`, `DB`, `Currency`, `SymbolAppear`, `Photo`, `DateModified`, `PerformedBy`) VALUES
(1, 'RAMADA PLAZA', 'XYZ', 'localhost', 'root', '', 'ramada', '$', 0, '1.png', '2016-07-13 15:02:42', 4);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `ID` int(11) NOT NULL,
  `FirstName` text NOT NULL,
  `LastName` text NOT NULL,
  `EmailAddress` text NOT NULL,
  `Password` char(32) NOT NULL,
  `Role` int(11) NOT NULL,
  `Status` tinyint(1) NOT NULL,
  `Photo` text NOT NULL,
  `DateAdded` datetime NOT NULL,
  `DateModified` datetime NOT NULL,
  `PerformedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`ID`, `FirstName`, `LastName`, `EmailAddress`, `Password`, `Role`, `Status`, `Photo`, `DateAdded`, `DateModified`, `PerformedBy`) VALUES
(4, 'Test', 'Admin', 'admin@admin.com', 'e14c05f0dc27e6be1fc127abaf474a59', 1, 1, '', '2015-08-29 10:50:00', '2016-07-19 05:24:36', 4),
(16, 'Test', 'Employee', 'employee@employee.com', '202cb962ac59075b964b07152d234b70', 2, 1, '16.png', '2016-06-11 12:34:52', '2016-06-14 10:58:07', 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `leads`
--
ALTER TABLE `leads`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `pg_about`
--
ALTER TABLE `pg_about`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `leads`
--
ALTER TABLE `leads`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `pg_about`
--
ALTER TABLE `pg_about`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
