<?php include("Common.php"); ?>
<?php include("CheckAdminLogin.php"); ?>
<?php
  $action = "";
  $msg = "";
  if(isset($_POST["action"]))
    $action = $_POST["action"];
if($action == "delete")
  {
    if(isset($_REQUEST["ids"]) && is_array($_REQUEST["ids"]))
    {
      
      foreach($_REQUEST["ids"] as $BID)
      {
      $row = mysql_query("SELECT File FROM multimedia  WHERE Type=1 AND ID = ". $BID ."") or die (
mysql_error());
      $dt = mysql_fetch_array($row);
      if(is_file(DIR_MULTIMEDIA_IMAGES . $dt['File']))
        unlink(DIR_MULTIMEDIA_IMAGES . $dt['File']);
      
      
      mysql_query("DELETE FROM multimedia  WHERE Type=1 AND ID IN (" . $BID . ")") or die (mysql_error());
      
      }
      
            
      $_SESSION["msg"]='<div class="alert alert-success alert-dismissable">
      <i class="fa fa-ban"></i>
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <b>Delete All selected Images.</b>
      </div>';
      redirect("MultimediaImages.php");
    
    }
    else
    {
      $msg='<div class="alert alert-danger alert-dismissable">
      <i class="fa fa-ban"></i>
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <b>Please select Image to delete.</b>
      </div>';
    }
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Multimedia Images</title>

	<!-- Bootstrap -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="build/css/custom.min.css" rel="stylesheet">
	
	
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        
		<?php include_once("Sidebar.php");

$prev="";
        $next="";
        $nav="";
        $rowsPerPage=20;
        $pageNum=1;
        
        $DFrom="";
        $DTo="";
        $DateFrom="";
        $DateTo="";
        
        $Keywords="";
        
        $SortField=2;
        $SortType="ASC";
          
        $_SORT_FIELDS = array("DateAdded");
        
        if(isset($_REQUEST["Keywords"]))
          $Keywords = trim($_REQUEST["Keywords"]);

        if(isset($_REQUEST["PageIndex"]) && ctype_digit(trim($_REQUEST["PageIndex"])))
          $pageNum=trim($_REQUEST["PageIndex"]);

        $offset = ($pageNum - 1) * $rowsPerPage;
        $limit=" Limit ".$offset.", ".$rowsPerPage;
        
        $query="SELECT ID,Name,File,DATE_FORMAT(DateAdded, '%D %b %Y<br>%r') AS Added
        FROM multimedia WHERE ID <>0 AND Type=1 ";
        if($Keywords != "")
          $query .= " AND (Name LIKE '%" . dbinput($Keywords) . "%')";
        
        
        
        $result = mysql_query ($query.$limit) or die("Could not select because: ".mysql_error()); 
        $num = mysql_num_rows($result);
        
        $r = mysql_query ($query) or die("Could not select because: ".mysql_error());
        $self = $_SERVER['PHP_SELF'];

        $maxRow = mysql_num_rows($r);
        if($maxRow > 0)
        { 
          $maxPage = ceil($maxRow/$rowsPerPage);
          $nav  = '';
          if($maxPage>1)
          for($page = 1; $page <= $maxPage; $page++)
          {
            
             if ($page == $pageNum)
              $nav .= "&nbsp;<li class=\"active\"><a href='#'>$page</a></li >"; 
             else
              $nav .= "&nbsp;<li ><a href=\"$self?PageIndex=$page&rpp=$rowsPerPage&Keywords=$Keywords&SortField=$SortField&SortType=$SortType&DFrom=$DFrom&DTo=$DTo\" class=\"lnk\">$page</a> </li>";
          }
          
          if ($pageNum > 1)
          {
            $page  = $pageNum - 1;
            $prev  = "&nbsp;<a href=\"$self?PageIndex=$page&rpp=$rowsPerPage&Keywords=$Keywords&SortField=$SortField&SortType=$SortType&DFrom=$DFrom&DTo=$DTo\" class=\"lnk\">Previous</a> ";
            $first = "&nbsp;<a href=\"$self?PageIndex=1&rpp=$rowsPerPage&Keywords=$Keywords&SortField=$SortField&SortType=$SortType&DFrom=$DFrom&DTo=$DTo\" class=\"lnk\">First</a> ";
          } 
          else
          {
             $prev  = ''; // we're on page one, don't print previous link
             $first = '&nbsp;First'; // nor the first page link
          }
          
          if ($pageNum < $maxPage)
          {
            $page = $pageNum + 1;
            $next = "&nbsp;<a href=\"$self?PageIndex=$page&rpp=$rowsPerPage&Keywords=$Keywords&SortField=$SortField&SortType=$SortType&DFrom=$DFrom&DTo=$DTo\" class=\"lnk\">Next</a> ";
            $last = "&nbsp;<a href=\"$self?PageIndex=$maxPage&rpp=$rowsPerPage&Keywords=$Keywords&SortField=$SortField&SortType=$SortType&DFrom=$DFrom&DTo=$DTo\" class=\"lnk\">Last Page</a> ";
          } 
          else
          {
             $next = "";
             $last = '&nbsp;Last'; // nor the last page link
          }
        }
    ?>

    ?>
		
		
		<?php include_once("Header.php"); ?>

       
		
		
        <!-- page content -->
        <div class="right_col" role="main" style="min-height:0px; !important">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Multimedia Images <small>Queries of clients</small></h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
             
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Multimedia Images</h2>
                    <ul class="nav navbar-right panel_toolbox">
					  <li><button class="btn btn-success margin" type="button" onClick="location.href='AddNewMultimediaImage.php'">Add New</button>
                <button type="button" class="btn bg-navy margin" onClick="javascript:doDelete()" style="margin-right:auto"<?php if($maxRow<=0) {echo ' disabled="disabled"';}?>>Delete</button></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
				   <?php
						echo $msg;
						if(isset($_SESSION["msg"]) && $_SESSION["msg"]!="")
						{
							echo $_SESSION["msg"];
							$_SESSION["msg"]="";
						}
					?>
                  <div class="x_content">
					<form id="frmPages"  method="post" action="<?php echo $self;?>">
                <input type="hidden" name="rpp" value="<?php echo $rowsPerPage; ?>" />
                <input type="hidden" name="PageIndex" value="<?php echo $pageNum; ?>" />
                <input type="hidden" id="action" name="action" value="" />
                <table id="dataTable" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th style="text-align:center"><input type="checkbox" name="chkAll" class="checkUncheckAll"></th>
                      <th>Name</th>
            <th>Image</th>
                      <th>Added</th>
                      <th>Link</th>
                    </tr>
                  </thead>
          
                  <tbody>
                    <?php
      if($maxRow==0)
      {
    ?>
                    <tr class="noPrint">
                      <td colspan="11" align="center" class="error"><b>No Image listed.</b></td>
                    </tr>
                    <?php 
      }
      else
      {
        ?>
        
        <?php
        while($row = mysql_fetch_array($result,MYSQL_ASSOC))
        {
    ?>
                    <tr>
                      <td align="center" class="noPrint"><input class="chkIds" type="checkbox" name="ids[]" value="<?php echo $row["ID"]; ?>" />
                        <input type="hidden" name="ids1[]" value="<?php echo $row["ID"]; ?>"></td>
                      <td><a href="javascript:getPages(<?php echo $row["ID"]; ?>, 20)" state="1" class="main-page<?php echo $row["ID"]; ?>"><?php echo dboutput($row["Name"]); ?></a></td>
            <td><img src="<?php echo DIR_MULTIMEDIA_IMAGES.dboutput($row['File']); ?>"  style="height:100px" /></td>
            <td><?php echo $row["Added"]; ?></td>
            <td class="noPrint"><input type="text" readonly value="<?php echo "http://".$_SERVER['HTTP_HOST'].'/admin/'.DIR_MULTIMEDIA_IMAGES.dboutput($row['File']);; ?>" style='width:500px;height:32px;color:#fff;background: #ed1c24 url("assets/copy.jpg") no-repeat right top;border: solid 2px black;'></td>
                    </tr>
                    <?php       
        }
      } 
      mysql_close($dbh);
    ?>
                  </tbody>
                </table>
              </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

		<?php include_once("Footer.php"); ?>
        
      </div>
    </div>

    <!-- jQuery -->
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="vendors/nprogress/nprogress.js"></script>
    <!-- Datatables -->
    <script src="vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script src="vendors/jszip/dist/jszip.min.js"></script>
    <script src="vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="build/js/custom.min.js"></script>

    <!-- Datatables -->
    <script>
      $(document).ready(function() {
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();
        $('#datatable-keytable').DataTable({
          keys: true
        });

      //  $('#datatable-responsive').DataTable();
		
		$('#datatable-responsive').DataTable({
				"order": [[ 1, "asc" ]],
				"aoColumnDefs": [ { "bSortable": false, "aTargets": [ 0, 7 ] } ],
				"iDisplayLength": 10,
				"aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]]
			});

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        var table = $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        TableManageButtons.init();
      });
    </script>
    <!-- /Datatables -->
	
	<script language="javascript">
	$(document).ready(function () {				
		$(".checkUncheckAll").click(function () {
			$(".chkIds").prop("checked", $(this).prop("checked"));			
		});
	});
	var counter = 0;
	
	
	function doDelete()
	{
		if($(".chkIds").is(":checked"))
		{
			if(confirm("Are you sure to delete."))
			{
				$("#action").val("delete");
				$("#frmPages").submit();
			}
		}
		else
			alert("Please select Multimedia Images to delete");
	}
	
	function SingleDelete(x)
	{
		if(confirm("Are you sure to delete."))
		{
			$("#action").val("SingleDelete");
			$("#DeleteID").val(x);
			$("#frmPages").submit();
		}
	}
	
</script>

  </body>
</html>