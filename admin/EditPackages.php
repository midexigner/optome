<?php
include_once("Common.php");
include("CheckAdminLogin.php");


	$msg="";
	$Name="";
	$subheading="";
	$Image="";
	$Status=1;
	$ID=0;
	$OfferPrice="";
	$OldPrice="";
	$Details='';
	
	$ID=0;
	if(isset($_REQUEST["ID"]) && ctype_digit(trim($_REQUEST["ID"])))
		$ID=trim($_REQUEST["ID"]);
		
if(isset($_POST["action"]) && $_POST["action"] == "submit_form")
{
	if(isset($_POST["Status"]) && ((int)$_POST["Status"] == 0 || (int)$_POST["Status"] == 1))
		$Status=trim($_POST["Status"]);	
	if(isset($_POST["Name"]))
		$Name=trim($_POST["Name"]);
	 if(isset($_POST["subheading"]))
		$subheading=trim($_POST["subheading"]);
	if(isset($_POST["OfferPrice"]))
		$OfferPrice = trim($_POST["OfferPrice"]);
	if(isset($_POST["OldPrice"]))
		$OldPrice = trim($_POST["OldPrice"]);
	 if(isset($_POST["Details"]))
	 	$Details=trim($_POST["Details"]);
	if(isset($_POST["flPage"]) && $_FILES["flPage"]['name'] != "")
	{
		$filenamearray=explode(".", $_FILES["flPage"]['name']);
		$ext=strtolower($filenamearray[sizeof($filenamearray)-1]);
	
		if(!in_array($ext, $_IMAGE_ALLOWED_TYPES))
		{
			$msg='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Only '.implode(", ", $_IMAGE_ALLOWED_TYPES) . ' files can be uploaded.</b>
			</div>';
		}			
		else if($_FILES["flPage"]['size'] > (MAX_IMAGE_SIZE*1024))
		{
			$msg='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Image size must be ' . MAX_IMAGE_SIZE . ' KB or less.</b>
			</div>';
		}
	}
	// if(isset($_POST["flPage1"]) && $_FILES["flPage1"]['name'] != "")
	// {
	// 	$filenamearray1=explode(".", $_FILES["flPage1"]['name']);
	// 	$ext1=strtolower($filenamearray1[sizeof($filenamearray1)-1]);
	
	// 	if(!in_array($ext1, $_IMAGE_ALLOWED_TYPES1))
	// 	{
	// 		$msg='<div class="alert alert-danger alert-dismissable">
	// 		<i class="fa fa-ban"></i>
	// 		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	// 		<b>Only '.implode(", ", $_IMAGE_ALLOWED_TYPES1) . ' files can be uploaded.</b>
	// 		</div>';
	// 	}			
	// 	else if($_FILES["flPage1"]['size'] > (MAX_IMAGE_SIZE*1024))
	// 	{
	// 		$msg='<div class="alert alert-danger alert-dismissable">
	// 		<i class="fa fa-ban"></i>
	// 		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	// 		<b>Image size must be ' . MAX_IMAGE_SIZE . ' KB or less.</b>
	// 		</div>';
	// 	}
	// }
	// if(isset($_POST["flPage2"]) && $_FILES["flPage2"]['name'] != "")
	// {
	// 	$filenamearray2=explode(".", $_FILES["flPage2"]['name']);
	// 	$ext2=strtolower($filenamearray2[sizeof($filenamearray2)-1]);
	
	// 	if(!in_array($ext2, $_IMAGE_ALLOWED_TYPES2))
	// 	{
	// 		$msg='<div class="alert alert-danger alert-dismissable">
	// 		<i class="fa fa-ban"></i>
	// 		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	// 		<b>Only '.implode(", ", $_IMAGE_ALLOWED_TYPES2) . ' files can be uploaded.</b>
	// 		</div>';
	// 	}			
	// 	else if($_FILES["flPage2"]['size'] > (MAX_IMAGE_SIZE*1024))
	// 	{
	// 		$msg='<div class="alert alert-danger alert-dismissable">
	// 		<i class="fa fa-ban"></i>
	// 		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	// 		<b>Image size must be ' . MAX_IMAGE_SIZE . ' KB or less.</b>
	// 		</div>';
	// 	}
	// }

	/*if($Heading == "")
	{
		$msg='<div class="alert alert-danger alert-dismissable">
		<i class="fa fa-ban"></i>
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<b>Please enter Caption Heading.</b>
		</div>';
	}
	else if($Text == "")
	{
		$msg='<div class="alert alert-danger alert-dismissable">
		<i class="fa fa-ban"></i>
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<b>Please enter Caption Para.</b>
		</div>';
	}
*/
	if($msg=="")
	{

		$query="UPDATE packages SET DateModified=NOW(),
				Name = '" . dbinput($Name) . "',
				subheading = '" . dbinput($subheading) . "',
				Status='".(int)$Status . "',
				OfferPrice = '" . dbinput($OfferPrice) . "',
				OldPrice = '" . dbinput($OldPrice) . "',
				Details = '" . $Details. "',
				PerformedBy = '" . dbinput($_SESSION['UserID']) . "' Where ID = ".$ID."";
		mysql_query($query) or die (mysql_error());
		// echo $query;
		//$ID = mysql_insert_id();
		$_SESSION["msg"]='<div class="alert alert-success alert-dismissable">
		<i class="fa fa-ban"></i>
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<b>User has been Updated.</b>
		</div>';		
		
		if(isset($_FILES["flPage"]) && $_FILES["flPage"]['name'] != "")
		{
			
		
			ini_set('memory_limit', '-1');
			
			$tempName = $_FILES["flPage"]['tmp_name'];
			$realName = $ID . "." . $ext;
			$StoreImage = $realName; 
			$target = DIR_SLIDERS . $realName;
			if(is_file(DIR_SLIDERS . $StoreImage))
				unlink(DIR_SLIDERS . $StoreImage);
			$moved=move_uploaded_file($tempName, $target);
		
			if($moved)
			{			
			
				$query="UPDATE packages SET Image='" . dbinput($realName) . "' WHERE  ID=" . (int)$ID;
				mysql_query($query) or die(mysql_error());
			}
			else
			{
				$_SESSION["msg"]='<div class="alert alert-warning alert-dismissable">
					<i class="fa fa-ban"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<b>User has been saved but Slider can not be uploaded.</b>
					</div>';
			}
		}
		
		
		
		redirect($_SERVER["PHP_SELF"].'?ID='.$ID);	
	}
		

}
else
{
	$query="SELECT * FROM packages WHERE  ID='" . (int)$ID . "'";
	
	$result = mysql_query ($query) or die(mysql_error()); 
	$num = mysql_num_rows($result);
	
	if($num==0)
	{
		$_SESSION["msg"]='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Invalid Packages ID.</b>
		</div>';
		redirect("Packages.php");
	}
	else
	{
		$row = mysql_fetch_array($result,MYSQL_ASSOC);
		
		$ID=$row["ID"];
		//$Role=$row["Role"];
		$Name=$row["Name"];
		$subheading=$row["subheading"];
		$Status=$row["Status"];
		$Image=$row["Image"];
		$OfferPrice=$row["OfferPrice"];
		$Details=$row["Details"];
	}
}


?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Edit Slider</title>

    <!-- Bootstrap -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    
    <!-- Custom Theme Style -->
    <link href="build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <?php include_once("Sidebar.php"); ?>

        <?php include_once("Header.php"); ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Edit Slider</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                              <button class="btn btn-default" type="button">Go!</button>
                          </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Update Form</h2>
                    <ul class="nav navbar-right panel_toolbox">
					  <li><a href="Sliders.php" class="btn btn-default active"><i class="fa fa-arrow-left"></i> Back</a></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
				  <?php
		  		//echo $msg;
				if(isset($_SESSION["msg"]))
				{
					echo $_SESSION["msg"];
					$_SESSION["msg"]="";
				}
				?>
                  <div class="x_content">

                    <form id="frmPages" action="<?php echo $_SERVER["PHP_SELF"];?>?ID=<?php echo $ID; ?>" method="post"  enctype="multipart/form-data" class="form-horizontal form-label-left" novalidate>
					  <input type="hidden" name="action" value="submit_form" />
					  <input type="hidden" name="ID" value="<?php echo $ID; ?>" />
                      <span class="section">Fill All Mandatory Fields</span>

					  	<div class="item form-group">
						  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Slider">Logo </label>
						  <div class="col-md-6 col-sm-6 col-xs-12">
						  <img src="<?php echo (is_file(DIR_PACKAGES . $Image) ? DIR_PACKAGES.$Image : 'images/avatar.png'); ?>" class="thumbnail" alt="" id="imgpreview" style="height:100px;" />
						  <input type="file" name="flPage" class="form-control col-md-7 col-xs-12" onchange="readURL(this);"/>
						  <p class="help-block">Image types allowed: jpg, jpeg, gif, png. --- Image size must be <?php echo MAX_IMAGE_SIZE/1024 ?> MB or less.</p>
						  </div>
						</div>
 <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Name">Name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="Name" class="form-control col-md-7 col-xs-12" data-validate-length-range="3" name="Name" required="required" type="text" value="<?php echo $Name; ?>">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="subheading">Sub Heading <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="subheading" class="form-control col-md-7 col-xs-12" data-validate-length-range="3" name="subheading" required="required" type="text" value="<?php echo $subheading; ?>">
                        </div>
                      </div>
                        <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="OfferPrice">Price <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="OfferPrice" class="form-control col-md-7 col-xs-12" data-validate-length-range="3" name="OfferPrice" required="required" type="text" value="<?php echo $OfferPrice; ?>">
                        </div>
                      </div>
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Details">Details <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea id="Details" class="form-control col-md-7 col-xs-12" data-validate-length-range="3" name="Details" required="required" ><?php echo $Details; ?></textarea>
                        </div>
                      </div>
					  <div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
						<div class="col-md-6 col-sm-6 col-xs-12" style="margin-top:8px;">
							<label>
							  <input type="radio" name="Status" value="1" <?php echo ($Status == '1' ? 'checked' : ''); ?>> &nbsp; Active &nbsp;
							</label>
							<label>
							  <input type="radio" name="Status" value="0" <?php echo ($Status == '0' ? 'checked' : ''); ?>> &nbsp; Deactive &nbsp;
							</label>
						</div>
					  </div>
					  
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <a href="Packages.php" class="btn btn-primary">Cancel</a>
                          <button id="send" type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <?php include_once("Footer.php"); ?>
      </div>
    </div>

    <!-- jQuery -->
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="vendors/nprogress/nprogress.js"></script>
    <!-- validator -->
    <script src="vendors/validator/validator.min.js"></script>
    <script src="js/tinymce/tinymce.min.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="build/js/custom.min.js"></script>

    <!-- validator -->
    <script>

    function readURL(input) {
	        if (input.files && input.files[0]) {
	            var reader = new FileReader();
	            reader.onload = function (e) {
	                $('#imgpreview')
	                    .attr('src', e.target.result)
	                    .width(400)
	                    .height(142);
	            };
	            reader.readAsDataURL(input.files[0]);
	        }
			else
	                $('#imgpreview').attr('src', '').width(0).height(0);
	    }
	    function readURL1(input) {
	        if (input.files && input.files[0]) {
	            var reader = new FileReader();
	            reader.onload = function (e) {
	                $('#imgpreview1')
	                    .attr('src', e.target.result)
	                    .width(400)
	                    .height(142);
	            };
	            reader.readAsDataURL(input.files[0]);
	        }
			else
	                $('#imgpreview1').attr('src', '').width(0).height(0);
	    }
	    function readURL2(input) {
	        if (input.files && input.files[0]) {
	            var reader = new FileReader();
	            reader.onload = function (e) {
	                $('#imgpreview2')
	                    .attr('src', e.target.result)
	                    .width(400)
	                    .height(142);
	            };
	            reader.readAsDataURL(input.files[0]);
	        }
			else
	                $('#imgpreview2').attr('src', '').width(0).height(0);
	    }
      // initialize the validator function
      validator.message.date = 'not a real date';

      // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
      $('form')
        .on('blur', 'input[required], input.optional, select.required', validator.checkField)
        .on('change', 'select.required', validator.checkField)
        .on('keypress', 'input[required][pattern]', validator.keypress);

      $('.multi.required').on('keyup blur', 'input', function() {
        validator.checkField.apply($(this).siblings().last()[0]);
      });

      $('form').submit(function(e) {
        e.preventDefault();
        var submit = true;

        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
          submit = false;
        }

        if (submit)
          this.submit();

        return false;
      });


          tinymce.init({
   selector: 'textarea',
   height: 200,
   menubar: false,
   branding: false,
  force_p_newlines : true,
force_br_newlines : false,
convert_newlines_to_brs : false,
remove_linebreaks : true,
forced_root_block : 'p', 
   plugins: [
     'advlist autolink lists link image charmap print preview anchor',
     'searchreplace visualblocks code fullscreen',
     'insertdatetime media table contextmenu paste code'
   ],
   toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | code',

 });
    </script>
    <!-- /validator -->
  </body>
</html>