<?php
include_once("Common.php");
include("CheckAdminLogin.php");


	$msg="";
	$Status=1;
	$ID=0;
	$Name="";
	$Contact="";
	$Passport="";
	$Address="";
	$CP="";
	$BkrName="";
	$BkrContact="";
	$Arrival="";
	$Departure="";
	$Adults=0;
	$Child=0;
	$Rooms=0;
	$RoomType="";
	$PaymentMethod="";
	$PickDropAirport="";
	$Overview="";
	
	$ID=0;
	if(isset($_REQUEST["ID"]) && ctype_digit(trim($_REQUEST["ID"])))
		$ID=trim($_REQUEST["ID"]);
		
if(isset($_POST["action"]) && $_POST["action"] == "submit_form")
{			
	if(isset($_REQUEST["ID"]) && ctype_digit(trim($_REQUEST["ID"])))
		$ID=trim($_REQUEST["ID"]);
	if(isset($_POST["Status"]) && ((int)$_POST["Status"] == 0 || (int)$_POST["Status"] == 1))
		$Status=trim($_POST["Status"]);	
	if(isset($_POST["Overview"]))
		$Overview=trim($_POST["Overview"]);


	if($msg=="")
	{

		$query="UPDATE leads SET DateModified=NOW(),
				Status='".(int)$Status . "',
				Overview = '" . dbinput($Overview) . "',
				PerformedBy = '" . dbinput($_SESSION['UserID']) . "' Where ID = ".$ID."";
		mysql_query($query) or die (mysql_error());
		// echo $query;
		//$ID = mysql_insert_id();
		$_SESSION["msg"]='<div class="alert alert-success alert-dismissable">
		<i class="fa fa-ban"></i>
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<b>Reservation has been Updated.</b>
		</div>';		
		
		redirect($_SERVER["PHP_SELF"].'?ID='.$ID);	
	}
		

}
else
{
	$query="SELECT ID,Name,Contact,Passport,Adults,Child,RoomType,Rooms,PickDropAirport,BkrContact,BkrName,Address,CP,PaymentMethod,Status,Overview,ID,DATE_FORMAT(Arrival, '%D %b %Y') AS Arrival,DATE_FORMAT(Departure, '%D %b %Y') AS Departure FROM leads WHERE  ID='" . (int)$ID . "'";
	
	$result = mysql_query ($query) or die(mysql_error()); 
	$num = mysql_num_rows($result);
	
	if($num==0)
	{
		$_SESSION["msg"]='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Invalid Reservation ID.</b>
		</div>';
		redirect("Reservations.php");
	}
	else
	{
		$row = mysql_fetch_array($result,MYSQL_ASSOC);
		
		$ID=$row["ID"];
		$Name=$row["Name"];
		$Contact=$row["Contact"];
		$Passport=$row["Passport"];
		$Address=$row["Address"];
		$CP=$row["CP"];
		$BkrName=$row["BkrName"];
		$BkrContact=$row["BkrContact"];
		$Arrival=$row["Arrival"];
		$Departure=$row["Departure"];
		$Adults=$row["Adults"];
		$Child=$row["Child"];
		$Rooms=$row["Rooms"];
		$RoomType=$row["RoomType"];
		$PaymentMethod=$row["PaymentMethod"];
		$PickDropAirport=$row["PickDropAirport"];
		$Status=$row["Status"];
		$Overview=$row["Overview"];
	}
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Edit Reservation</title>

    <!-- Bootstrap -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    
    <!-- Custom Theme Style -->
    <link href="build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <?php include_once("Sidebar.php"); ?>

        <?php include_once("Header.php"); ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Edit Reservation</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                              <button class="btn btn-default" type="button">Go!</button>
                          </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Update Form</h2>
                    <ul class="nav navbar-right panel_toolbox">
					  <li><a href="Reservations.php" class="btn btn-default active"><i class="fa fa-arrow-left"></i> Back</a></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
				  <?php
		  		//echo $msg;
				if(isset($_SESSION["msg"]))
				{
					echo $_SESSION["msg"];
					$_SESSION["msg"]="";
				}
				?>
                  <div class="x_content">

                    <form id="frmPages" action="<?php echo $_SERVER["PHP_SELF"];?>?ID=<?php echo $ID; ?>" method="post"  enctype="multipart/form-data" class="form-horizontal form-label-left" novalidate>
					  <input type="hidden" name="action" value="submit_form" />
					  <input type="hidden" name="ID" value="<?php echo $ID; ?>" />
                      <span class="section">Reservation Information</span>

					  
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Arrival">Arrival</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="Arrival" disabled class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $Arrival; ?>">
                        </div>
                      </div>
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Departure">Departure</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="Departure" disabled class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $Departure; ?>">
                        </div>
                      </div>
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Adults">Adults</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="Adults" disabled class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $Adults; ?>">
                        </div>
                      </div>
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Child">Child</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="Child" disabled class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $Child; ?>">
                        </div>
                      </div>
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Rooms">No. Of Rooms</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="Rooms" disabled class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $Rooms; ?>">
                        </div>
                      </div>
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="RoomType">Room Type</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="RoomType" disabled class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $RoomType; ?>">
                        </div>
                      </div>
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="PaymentMethod">Payment Method</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="PaymentMethod" disabled class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $PaymentMethod; ?>">
                        </div>
                      </div>
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="PickDropAirport">Pick and Drop from Airport</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="PickDropAirport" disabled class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $PickDropAirport; ?>">
                        </div>
                      </div>
					  <span class="section">Guest Information</span>
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Name">Guest Name</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="Name" disabled class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $Name; ?>">
                        </div>
                      </div>
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Contact">Guest Contact</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="Contact" disabled class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $Contact; ?>">
                        </div>
                      </div>
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Passport">Guest Passport</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="Passport" disabled class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $Passport; ?>">
                        </div>
                      </div>
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Address">Address</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="Address" disabled class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $Address; ?>">
                        </div>
                      </div>
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="CP">Company / Personal</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="CP" disabled class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $CP; ?>">
                        </div>
                      </div>
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="BkrName">Booker Name</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="BkrName" disabled class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $BkrName; ?>">
                        </div>
                      </div>
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="BkrContact">Booker Contact</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="BkrContact" disabled class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $BkrContact; ?>">
                        </div>
                      </div>
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Overview">Overview <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea id="Overview" class="form-control col-md-7 col-xs-12" data-validate-length-range="5" name="Overview" required="required"><?php echo $Overview; ?></textarea>
                        </div>
                      </div>
					  <div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
						<div class="col-md-6 col-sm-6 col-xs-12" style="margin-top:8px;">
							<label>
							  <input type="radio" name="Status" value="1" <?php echo ($Status == '1' ? 'checked' : ''); ?>> &nbsp; Checked &nbsp;
							</label>
							<label>
							  <input type="radio" name="Status" value="0" <?php echo ($Status == '0' ? 'checked' : ''); ?>> &nbsp; Pending &nbsp;
							</label>
						</div>
					  </div>
					  
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <a href="Reservations.php" class="btn btn-primary">Cancel</a>
                          <button id="send" type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <?php include_once("Footer.php"); ?>
      </div>
    </div>

    <!-- jQuery -->
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="vendors/nprogress/nprogress.js"></script>
    <!-- validator -->
    <script src="vendors/validator/validator.min.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="build/js/custom.min.js"></script>

    <!-- validator -->
    <script>
      // initialize the validator function
      validator.message.date = 'not a real date';

      // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
      $('form')
        .on('blur', 'input[required], input.optional, select.required', validator.checkField)
        .on('change', 'select.required', validator.checkField)
        .on('keypress', 'input[required][pattern]', validator.keypress);

      $('.multi.required').on('keyup blur', 'input', function() {
        validator.checkField.apply($(this).siblings().last()[0]);
      });

      $('form').submit(function(e) {
        e.preventDefault();
        var submit = true;

        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
          submit = false;
        }

        if (submit)
          this.submit();

        return false;
      });
    </script>
    <!-- /validator -->
  </body>
</html>