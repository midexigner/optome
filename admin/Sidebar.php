<div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">			  
			  <a href="index.php" class="site_title"><img src="<?php echo (is_file(DIR_SETTINGS . $SETTING_Photo) ? DIR_SETTINGS . $SETTING_Photo : 'images/avatar2.png'); ?>" style="height:50px;" /> <span style="display: none;"><?php echo $SETTING_Name; ?></span></a>
            </div> 
            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile">
              <div class="profile_pic">
					<img src="<?php echo (is_file(DIR_ADMINUSERS . $_SESSION['Photo']) ? DIR_ADMINUSERS.$_SESSION["Photo"] : 'images/avatar.png'); ?>" class="img-circle profile_img" alt="<?php echo $_SESSION['UserFullName']; ?>" />
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?php echo $_SESSION['UserFullName']; ?> </h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <ul class="nav side-menu">
				  <h3>&emsp;</h3>
				  <?php if($_SESSION['RoleID'] == 10){ ?>
				  <li><a href="MyDashboard.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
				  <?php }else if($_SESSION['RoleID'] == 1){ ?>
				  <li><a href="Dashboard.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
				  <li><a href="AdminUsers.php"><i class="fa fa-user"></i> Admin Users</a></li>
                  <li><a><i class="fa fa-desktop"></i> CMS <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
					  <li><a href="Sliders.php"><i class="fa fa-desktop"></i> Sliders</a></li>
             <li><a href="Pages.php"><i class="fa fa-file"></i>Pages</a></li>
            <li><a href="Banners.php"><i class="fa fa-play"></i> Banners</a></li>
            <li><a href="About.php"><i class="fa fa-pencil"></i> About Page</a></li>
            <li><a href="HomePageService.php"><i class="fa fa-play"></i> Home Page Service </a></li>
            <li><a href="Downloads.php"><i class="fa fa-circle-o"></i> Download</a></li>
             <li><a href="Packages.php"><i class="fa fa-circle-o"></i> Packages</a></li>
              <li><a href="heading-text.php"><i class="fa fa-circle-o"></i> Heading &amp; Text </a></li>
           
					 <!-- <li><a href="Categories.php"><i class="fa fa-circle-o"></i> Categories</a></li>
            <li><a href="Products.php"><i class="fa fa-play"></i> Products</a></li>
            
            
             <li><a href="Video.php"><i class="fa fa-play"></i> Video</a></li>
					  <li><a href="Facilities.php"><i class="fa fa-circle-o"></i> Facilities</a></li>
					  <li><a href="Deals.php"><i class="fa fa-circle-o"></i> Deals & Discounts</a></li>
					  <li><a href="News.php"><i class="fa fa-newspaper-o"></i> News & Events</a></li>
					  <li><a href="Restaurants.php"><i class="fa fa-table"></i> Restaurants</a></li>
					  <li><a href="Rooms.php"><i class="fa fa-bed"></i> Rooms</a></li>
					  <li><a href="Banquets.php"><i class="fa fa-home"></i> Banquets</a></li>
					  <li><a href="Pages.php"><i class="fa fa-pencil"></i> Pages</a></li>
					  <li><a href="Home.php"><i class="fa fa-desktop"></i> Home Page</a></li>
					  <li><a href="About.php"><i class="fa fa-pencil"></i> About Page</a></li>
					  <li><a href="GoldCard.php"><i class="fa fa-cc"></i> Gold Card</a></li>
					  <li><a href="SocialLinks.php"><i class="fa fa-facebook"></i> Social Links</a></li> -->
					  
                      <li><a href="SocialLinks.php"><i class="fa fa-facebook"></i> Social Links</a></li>
                    </ul>
                  </li>
                   <li><a href="MultimediaImages.php"><i class="fa fa-upload"></i>  Multimedia Images</a></li>
                
                   <li><a href="Vacancy.php"><i class="fa fa-circle-o"></i> Vacancy</a></li>
				  <!-- <li><a href="Careers.php"><i class="fa fa-graduation-cap"></i> Careers</a></li> -->
				  <!-- <li><a href="Reservations.php"><i class="fa fa-globe"></i> Reservations</a></li> -->
				  <li><a href="contactus.php"><i class="fa fa-comments"></i> Contact Messages</a></li>
          <li><a href="Partners.php"><i class="fa fa-comments"></i> Partner Messages</a></li>
          <li><a href="Newsletter.php"><i class="fa fa-comments"></i> Newsletter</a></li>
           <li><a href="Career.php"><i class="fa fa-comments"></i> Career</a></li>

          
				  <?php } ?>
				  
                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
			  <?php if($_SESSION['RoleID'] == 1){ ?>
			  <a data-toggle="tooltip" data-placement="top" title="Settings" href="Settings.php">
                <span class="fa fa-gear" aria-hidden="true"></span>
              </a>
			  <a data-toggle="tooltip" data-placement="top" title="Database" id="Database" onClick="javascript:doBackup()">
                <span class="fa fa-database" aria-hidden="true"></span>
              </a>
			  <?php }else{ ?>
			  <a data-toggle="tooltip" data-placement="top" title="Settings" onClick="javascript:UnAuthorized()">
                <span class="fa fa-gear" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Database" id="Database" onClick="javascript:UnAuthorized()">
                <span class="fa fa-database" aria-hidden="true"></span>
              </a>
			  <?php } ?>
              <a data-toggle="tooltip" data-placement="top" title="Lock" href="<?php echo ($_SESSION['RoleID'] == 10 ? 'ClientLockNow.php' : 'LockNow.php') ?>">
                <span class="fa fa-eye-slash" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?php echo ($_SESSION['RoleID'] == 10 ? 'ClientLogout.php' : 'Logout.php') ?>">
                <span class="fa fa-sign-out" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>
		