<?php 
require_once 'admin/Common.php'; 
require_once 'include/head.php';
require_once 'include/header.php';
$Name ='';
$Email ='';
$Subject='';
$Phone='';
$Des ='';
$msg ='';
if(isset($_POST["action"]) && $_POST["action"] == "submit_form")
{
if(isset($_POST["name"]))
        $Name=trim($_POST["name"]);
    if(isset($_POST["email"]))
        $Email=trim($_POST["email"]);
      if(isset($_POST["phone"]))
        $Phone=trim($_POST["phone"]);
    if(isset($_POST["subject"]))
        $Subject=trim($_POST["subject"]);
     if(isset($_POST["message"]))
        $Des=trim($_POST["message"]);
        if($Name == "")
        {
            $msg='<div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Please enter Name.</b>
            </div>';
        }
        else if($Email == "")
        {
            $msg='<div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Please enter Email.</b>
            </div>';
        }
        else if($Subject == "")
        {
            $msg='<div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Please enter Company Name.</b>
            </div>';
        }
        else if($Des == "")
        {
            $msg='<div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b>Please enter Description.</b>
            </div>';
        }

        $headers = "From : " . $to;
        $message = "
        Name: ".'$Name'."
        \r\nEmail: ".'$Email'."
        \r\nSubject: ".'$Subject'."
        \r\nDescription: ".'$Des'."
        ";
   if($msg=="" && mail($to, $Name, $message, $headers))
    {
        $query="INSERT INTO contacts SET DateAdded=NOW(),
                Name = '" . dbinput($Name) . "',
                Email = '" . dbinput($Email) . "',
                Phone = '" . dbinput($Phone) . "',
                Subject = '" . dbinput($Subject) . "',
                Message = '" . dbinput($Des) . "'";
        mysql_query($query) or die (mysql_error());
        // echo $query;
        $ID = mysql_insert_id();
        $_SESSION["msg"]=' <p class="success alert alert-success"><i class="fa fa-check"></i> Your message has been sent successfully. </p>';
    }
}

$packagesHeadBgQ = mysql_query('SELECT * FROM `banners` WHERE ID = 11');
$packageHeadRowBg = mysql_fetch_assoc($packagesHeadBgQ);
?>

<style>

  .contact-breadscrumb {
    background-image: url(<?php echo SITE_URL .'/admin/'.DIR_BANNERS.$packageHeadRowBg['Image'] ?>) !important;
}
</style>

<section id="apus-breadscrumb" class="apus-breadscrumb contact-breadscrumb">
         <div class="container">
            <div class="row">
               <div class="wrapper-breads">
                  <div class="breadscrumb-inner">
                     <ol class="breadcrumb">
                        <li><a href="<?php echo SITE_URL; ?>">Home</a>  </li>
                        <li class="active">Contact Us</li>
                     </ol>
                     <h2 class="bread-title">Contact Us</h2>
                  </div>
               </div>
            </div>
         </div>
      </section>

<section id="contact-us" class="contact-section section-padding">
            <div class="container">
              <div class="page-header section-header">
                <h2>Get In Touch With Us!</h2>
                <samp class="line text-center"></samp>
                <span>Have an inquiry or need to find out about OptoMe services? Send us a note utilizing the frame beneath and somebody from the OptoMe team will be in touch soon. What's more, as usual, thank you for picking the best Fiber Broadband service.</span> 
              </div>
            </div>
            <div class="container form-section">
              <div class="row">
                <div class="form col-md-6 contact-form wow fadeUp animated" style="visibility: visible;">
                  <form class="form-horizontal form" action="<?php echo $self;?>" id="contact" method="post">
                       <input type="hidden" id="action" name="action" value="submit_form" />
                    <!-- IF MAIL SENT SUCCESSFULLY -->
                    <?php
                
                if(isset($_SESSION["msg"]))
                {
                    echo $_SESSION["msg"];
                    $_SESSION["msg"]="";
                }
                ?>
                    
                  
                   
                    <div class="form-group">
                      <div class="col-sm-12">
                        <input class="form-control" id="mi-name" type="text" name="name" placeholder="Name *" data-error="Please insert the Name">
                        <div class="help-block with-errors"></div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-12">
                        <input class="form-control" id="email" type="email" name="email" placeholder="Email *" data-error="Please insert the email address">
                         <div class="help-block with-errors"></div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-12">
                        <input class="form-control" id="mi-phone" type="text" name="phone" placeholder="Phone *" data-error="Please insert the Phone">
                         <div class="help-block with-errors"></div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-12">
                        <input class="form-control" id="mi-subject" type="text" name="subject" placeholder="Subject *" data-error="Please insert the Subject">
                         <div class="help-block with-errors"></div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-12 ">
                        <textarea class="form-control custom-control" id="mi-message" rows="4" name="message" placeholder="How can i help? *" data-error="Please insert the fill"></textarea>
                         <div class="help-block with-errors"></div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-11">
                        <button type="submit" id="cf-submit" name="submit" class="btn-theme btn">SUBMIT</button>
                      </div>
                    </div>
                  </form>
                </div>
                <div class="contact-detail col-md-6 col-xs-12 wow fadeUp animated" style="visibility: visible;">

                  <?php 

                    $query="SELECT ID,Website,Email,Phone,Mobile,Fax,Address,FooterAbout FROM settings WHERE ID <>0";
    $result = mysql_query ($query) or die(mysql_error()); 
    $num = mysql_num_rows($result);
   $row=mysql_fetch_array($result);
   $Website = $row["Website"];
   $EEmail = $row["Email"];
   $Phones = $row["Phone"];
   $Mobile = $row["Mobile"];
   $Fax = $row["Fax"];
   $Address = $row["Address"];
   $FooterAbout = $row["FooterAbout"];

                     ?>
                  <h3>Contact Information</h3>
                  <ul class="contact-detail">
                   <!--  <li>
                      <h6>Business Hour</h6>
                    
                      <p><i class="fa fa-clock-o"></i><?php echo $Fax; ?>
                    </p></li> -->
                    <li>
                      <h6>Office Address </h6>
                      <p><i class="fa fa-map-marker"></i><?php echo $Address; ?></p>
                    </li>
                    <li>
                      <h6>E-mail</h6>
                      <p><i class="fa fa-envelope-o"></i>
            <?php $eemail = explode(',', $EEmail);  
           
            ?><!-- -->

                        <a href="mailto:<?php echo @$eemail[0]; ?>"><?php echo @$eemail[0]; ?></a> , <a href="<?php echo @$eemail[1]; ?>"><?php echo @$eemail[1]; ?></a></p>
                       
                    </li>
                    <li>
                      <h6>Website</h6>
                      <p><i class="fa fa fa-link"></i><?php echo $Website; ?></p>
                    </li>
                    <li>
                      <h6>UAN</h6>
                      <p><a href="tel:<?php echo $Phone; ?>"><i class="fa fa-phone"></i><?php echo $Mobile; ?></a></p>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </section>

           <!-- Contact Section end Here -->
          <!-- Map Section Start Here -->
          <div class="map">
             <?php echo $FooterAbout; ?>
                  
          </div>
          <!-- Map Section Start Here -->

<?php 

require_once 'include/footer.php';
require_once 'include/foot.php';



 ?>

  