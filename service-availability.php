<?php 
require_once 'admin/Common.php';
require_once 'include/head.php';
require_once 'include/header.php';
$packagesHeadBgQ = mysql_query('SELECT * FROM `banners` WHERE ID = 11');
$packageHeadRowBg = mysql_fetch_assoc($packagesHeadBgQ);
?>

<style>

  .service-availability-breadcrumb {
    background-image: url(<?php echo SITE_URL .'/admin/'.DIR_BANNERS.$packageHeadRowBg['Image'] ?>) !important;
}
</style>

<section id="apus-breadscrumb" class="apus-breadscrumb service-availability-breadcrumb">
         <div class="container">
            <div class="row">
               <div class="wrapper-breads">
                  <div class="breadscrumb-inner">
                     <ol class="breadcrumb">
                        <li><a href="index.php">Home</a>  </li>
                        <li class="active">Service Availability</li>
                     </ol>
                     <h2 class="bread-title">Service Availability</h2>
                  </div>
               </div>
            </div>
         </div>
      </section>


<section id="pricing" class="pricing-section">
          <div class="section-padding">
            <div class="container">
              <div class="row">
                <div class="col-md-12">
                  <div class="page-header section-header text-center">
                    <h1 class="h-light">Awesome</h1>
                    <h1 class="h-bold">Service <span>Availablility</span></h1>
                    <span class="line text-center"></span>
                    <p>Our premium services are branched throughout Karachi including</p>
                  </div>
                </div>
              </div>

              <div class="row">
           <div id="map"></div>      
  <!--               <div>
  
    Nav tabs
    <ul class="nav nav-tabs" role="tablist">
      <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Home</a></li>
      <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>
      <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Messages</a></li>
      <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Settings</a></li>
    </ul>
  
    Tab panes
    <div class="tab-content">
      <div role="tabpanel" class="tab-pane fade active" id="home"> 
       
  
      </div>
      <div role="tabpanel" class="tab-pane fade" id="profile">...</div>
      <div role="tabpanel" class="tab-pane fade" id="messages">...</div>
      <div role="tabpanel" class="tab-pane fade" id="settings">...</div>
    </div>
  
  </div> -->
              </div>
              
            </div>
          </div>
        </section>



<?php 

require_once 'include/footer.php';
require_once 'include/foot.php';

 ?>
 <script>

      // This example creates a simple polygon representing the Bermuda Triangle.
      // When the user clicks on the polygon an info window opens, showing
      // information about the polygon's coordinates.

      var map;
      var infoWindow;

      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
           zoom: 13,
          center: {lat: 24.821630695748, lng: 67.070237457065},
          mapTypeId: 'terrain'
        });

        // Define the LatLng coordinates for the polygon.
        var triangleCoords = [
            { lat:24.778961,lng:67.056813},
{ lat:24.800458,lng:67.077133},
{ lat:24.801266,lng:67.076426},
{ lat:24.803477,lng:67.077987},
{ lat:24.807519,lng:67.078797},
{ lat:24.811318,lng:67.078336},
{ lat:24.816488,lng:67.07774},
{ lat:24.820642,lng:67.077159},
{ lat:24.829934,lng:67.075811},
{ lat:24.81974,lng:67.077369},
{ lat:24.814662,lng:67.077976},
{ lat:24.811782,lng:67.07828},
{ lat:24.821756,lng:67.077058},
{ lat:24.8313,lng:67.075558},
{ lat:24.831154,lng:67.074597},
{ lat:24.831134,lng:67.074021},
{ lat:24.830998,lng:67.073695},
{ lat:24.830921,lng:67.073326},
{ lat:24.830863,lng:67.073054},
{ lat:24.831007,lng:67.073754},
{ lat:24.831121,lng:67.07423},
{ lat:24.831214,lng:67.07459},
{ lat:24.831312,lng:67.075688},
{ lat:24.825509,lng:67.07642},
{ lat:24.817408,lng:67.077521},
{ lat:24.831093,lng:67.075652},
{ lat:24.831331,lng:67.075683},
{ lat:24.831257,lng:67.075619},
{ lat:24.831288,lng:67.075565},
{ lat:24.83128,lng:67.075404},
{ lat:24.831284,lng:67.07535},
{ lat:24.831295,lng:67.075646},
{ lat:24.831253,lng:67.075757},
{ lat:24.831134,lng:67.074581},
{ lat:24.831143,lng:67.073947},
{ lat:24.830958,lng:67.073518},
{ lat:24.831143,lng:67.074162},
{ lat:24.831113,lng:67.073958},
{ lat:24.831026,lng:67.073679},
{ lat:24.830558,lng:67.072306},
{ lat:24.830909,lng:67.07325},
{ lat:24.830364,lng:67.071276},
{ lat:24.830442,lng:67.071876},
{ lat:24.831065,lng:67.073593},
{ lat:24.831414,lng:67.075567},
{ lat:24.830831,lng:67.072972},
{ lat:24.830992,lng:67.073433},
{ lat:24.83118,lng:67.075691},
{ lat:24.828508,lng:67.076005},
{ lat:24.825836,lng:67.07649},
{ lat:24.822881,lng:67.076673},
{ lat:24.83122,lng:67.075665},
{ lat:24.829835,lng:67.069123},
{ lat:24.829346,lng:67.067484},
{ lat:24.829042,lng:67.064837},
{ lat:24.828738,lng:67.063219},
{ lat:24.828105,lng:67.057222},
{ lat:24.827823,lng:67.056009},
{ lat:24.827628,lng:67.054743},
{ lat:24.827531,lng:67.054152},
{ lat:24.827667,lng:67.053562},
{ lat:24.828884,lng:67.051369},
{ lat:24.826975,lng:67.051087},
{ lat:24.826557,lng:67.049273},
{ lat:24.826961,lng:67.049095},
{ lat:24.826686,lng:67.048008},
{ lat:24.826334,lng:67.048227},
{ lat:24.824269,lng:67.048754},
{ lat:24.823967,lng:67.047462},
{ lat:24.822613,lng:67.047651},
{ lat:24.822163,lng:67.047753},
{ lat:24.821099,lng:67.048037},
{ lat:24.819765,lng:67.048405},
{ lat:24.819548,lng:67.048221},
{ lat:24.81866,lng:67.049174},
{ lat:24.813849,lng:67.04617},
{ lat:24.816138,lng:67.042924},
{ lat:24.817199,lng:67.043195},
{ lat:24.817764,lng:67.041346},
{ lat:24.818046,lng:67.041333},
{ lat:24.81847,lng:67.040147},
{ lat:24.819013,lng:67.04024},
{ lat:24.819801,lng:67.037814},
{ lat:24.821041,lng:67.038285},
{ lat:24.821036,lng:67.035709},
{ lat:24.822627,lng:67.036373},
{ lat:24.825565,lng:67.037697},
{ lat:24.825808,lng:67.040856},
{ lat:24.826674,lng:67.042728},
{ lat:24.827482,lng:67.042292},
{ lat:24.827764,lng:67.041428},
{ lat:24.827567,lng:67.037166},
{ lat:24.827475,lng:67.035512},
{ lat:24.827129,lng:67.035231},
{ lat:24.825112,lng:67.035593},
{ lat:24.823057,lng:67.035074},
{ lat:24.822059,lng:67.034713},
{ lat:24.821567,lng:67.034577},
{ lat:24.820892,lng:67.034532},
{ lat:24.820588,lng:67.034175},
{ lat:24.816264,lng:67.03379},
{ lat:24.815739,lng:67.037062},
{ lat:24.813625,lng:67.036638},
{ lat:24.812549,lng:67.037477},
{ lat:24.80824,lng:67.035141},
{ lat:24.810733,lng:67.032051},
{ lat:24.804811,lng:67.028275}
        ];

        // Construct the polygon.
        var miMap = new google.maps.Polygon({
          paths: triangleCoords,
           strokeColor: '#00519d',
          strokeOpacity: 0.8,
          strokeWeight: 3,
           fillColor: '#FF0000',
          fillOpacity: 0.35
        });
        miMap.setMap(map);

        // Add a listener for the click event.
        miMap.addListener('click', showArrays);

        infoWindow = new google.maps.InfoWindow;
      }

      /** @this {google.maps.Polygon} */
      function showArrays(event) {
        // Since this polygon has only one path, we can call getPath() to return the
        // MVCArray of LatLngs.
        var vertices = this.getPath();

        var contentString = '<b>DHA Phase 6</b><br>' +
               '<br>';

        // Replace the info window's content and position.
        infoWindow.setContent(contentString);
        infoWindow.setPosition(event.latLng);

        infoWindow.open(map);
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDFytTt1KdVB7pxRVZ_XlCcQe6RZ6UZKCk&callback=initMap">
    </script>
    
  