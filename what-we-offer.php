<?php
require_once 'admin/Common.php'; 
require_once 'include/head.php';
require_once 'include/header.php';
$packagesHeadBgQ = mysql_query('SELECT * FROM `banners` WHERE ID = 11');
$packageHeadRowBg = mysql_fetch_assoc($packagesHeadBgQ);
?>

<style>

  .apus-breadscrumb {
    background-image: url(<?php echo SITE_URL .'/admin/'.DIR_BANNERS.$packageHeadRowBg['Image'] ?>) !important;
}
</style>

<section id="apus-breadscrumb" class="apus-breadscrumb">
         <div class="container">
            <div class="row">
               <div class="wrapper-breads">
                  <div class="breadscrumb-inner">
                     <ol class="breadcrumb">
                        <li><a href="index.php">Home</a>  </li>
                        <li class="active">What We Offer</li>
                     </ol>
                     <h2 class="bread-title">What We Offer</h2>
                  </div>
               </div>
            </div>
         </div>
      </section>


<section id="internet" class="about-section internet-section">
  <?php 
$internetQ = mysql_query("SELECT * FROM `pages` WHERE Status =1 AND ID = 1");
$internetRow = mysql_fetch_assoc($internetQ);
$name = explode(' ', $internetRow['Tagline']);
$last = count($name);
   ?>
          <div class="section-padding">
            <div class="container">
              <div class="row about-list" id="internet-us">
                <div class="col-md-6 col-xs-12 item">
                  <div class="page-header section-header">
                    <h2><?php echo $internetRow['Heading'].' <span>'.$internetRow['Tagline'].'</span>';?></h2>
                    <samp class="line text-center"></samp>
                  </div>
                  <div class="m-t-20"></div>
                 <?php echo $internetRow['Text']; ?>
                </div>
                 <div class="col-md-6 item"> 
                  <img src="<?php echo SITE_URL.'/admin/'.DIR_PAGES.$internetRow['Image']; ?>" alt="<?php echo $internetRow['Tagline']; ?>" class="img-responsive">
                </div>
              </div>
            </div>
          </div>
        </section>


        <section id="tv" class="about-section tv-section">
            <?php 
$internetQ = mysql_query("SELECT * FROM `pages` WHERE Status =1 AND ID = 2");
$internetRow = mysql_fetch_assoc($internetQ);
$name = explode(' ', $internetRow['Tagline']);
$last = count($name);
   ?>
          <div class="section-padding">
            <div class="container">
              <div class="row about-list">
                 <div class="col-md-6 item"> 
                 <img src="<?php echo SITE_URL.'/admin/'.DIR_PAGES.$internetRow['Image']; ?>" alt="<?php echo $internetRow['Tagline']; ?>" class="img-responsive">
                </div>
                <div class="col-md-6 col-xs-12 item">
                  <div class="page-header section-header">
                    <h2><?php echo $internetRow['Heading'].' <span>'.$internetRow['Tagline'].'</span>';?></h2>
                    <samp class="line text-center"></samp>
                  </div>
                  <div class="m-t-20"></div>
                     <?php echo $internetRow['Text']; ?>
                </div>
                
              </div>
            </div>
          </div>
        </section>




        <section id="telephone" class="about-section telephone-section">
                <?php 
$internetQ = mysql_query("SELECT * FROM `pages` WHERE Status =1 AND ID = 3");
$internetRow = mysql_fetch_assoc($internetQ);
$name = explode(' ', $internetRow['Tagline']);
$last = count($name);
   ?>
          <div class="section-padding">
            <div class="container">
              <div class="row about-list">
                <div class="col-md-6 col-xs-12 item">
                  <div class="page-header section-header">
                    <h2><?php echo $internetRow['Heading'].' <span>'.$internetRow['Tagline'].'</span>';?></h2>
                    <samp class="line text-center"></samp>
                  </div>
                  <div class="m-t-20"></div>
                    <?php echo $internetRow['Text']; ?>
                </div>
                 <div class="col-md-6 item"> 
                  <img src="<?php echo SITE_URL.'/admin/'.DIR_PAGES.$internetRow['Image']; ?>" alt="<?php echo $internetRow['Tagline']; ?>" class="img-responsive">
                </div>
              </div>
            </div>
          </div>
        </section>


       

<?php 

require_once 'include/footer.php';
require_once 'include/foot.php';

 ?>
  