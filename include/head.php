
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Optome</title>
     <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,700" rel="stylesheet" type="text/css">
      <!-- Normalize -->
      <link href="assets/css/normalize.css" rel="stylesheet" type="text/css">
      <!-- Bootstrap -->
      <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
      <link href="assets/css/hoverme-effect.css" rel="stylesheet" type="text/css">
      <link href="assets/css/bootstrap-theme.css" rel="stylesheet" type="text/css">
      <!-- fontawesome-->
      <link href="assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
      <link rel="stylesheet" type="text/css" href="assets/css/font-monia.css">
      <!-- owlcarousel-->
      <!-- <link rel="stylesheet" href="assets/owlcarousel/assets/owl.carousel.min.css">
      <link rel="stylesheet" href="assets/owlcarousel/assets/owl.theme.default.min.css"> -->
       <!-- slick slider-->
      <link rel="stylesheet" type="text/css" href="assets/slick/slick.css"/>
  <link rel="stylesheet" type="text/css" href="assets/slick/slick-theme.css"/>
      <!-- Swioppper  -->
     <!--  <link rel="stylesheet" href="assets/swipper/css/swiper.min.css"> -->
      <!-- animated-->
      <link href="assets/css/animate.css" rel="stylesheet" type="text/css">
        <!-- custom -->
      <link href="assets/css/style.css" rel="stylesheet" type="text/css">
              

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
      <script src="assets/js/vendor/modernizr.js"></script>
  </head>
  <body>