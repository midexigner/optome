
<div class="socialBoxRight">
  <ul class="list-unstyled">
    <li><a href=""><img src="assets/img/social/icon-facebook.png"></a></li>
    <li><a href=""><img src="assets/img/social/icon-instagram.png"></a></li>
    <li><a href=""><img src="assets/img/social/icon-twitter.png"></a></li>
    <li><a href=""><img src="assets/img/social/icon-linkedin.png"></a></li>

  </ul>
  

</div>

<div class="navbar-wrapper"> 

      
<!--      // being header -->
    
<header>
  <!--   // being topHeader -->
      <div id="topHeader">
      <div class="container">
    <div class="row">
        <div class="col-md-2 col-md-offset-10 box-top col-sm-6 col-xs-8 col-xs-offset-7 col-sm-offset-4">
        <ul class="list-inline text-center">
         
            <li><a href="login.php">LOGIN</a></li>
             <li><a href="get-optome.php">GET OPTOME</a></li>
            </ul>
        </div>
          </div>
           </div>
      </div> 
<!--   // end topHeader  -->
        
        <nav class="navbar navbar-default">
        <div class="container">
      <div class="row">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-menu" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.php"><img src="assets/img/logo.png" class="img-responsive"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="main-menu">
      <ul class="nav navbar-nav navbar-right">
        <li class="actives">
          <a href="<?php echo SITE_URL; ?>">Home</a></li>
        <li><a href="<?php echo SITE_URL; ?>/company">Company</a></li>
       
        <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">Services <b class="caret"></b></a> 
                      
                        <ul class="dropdown-menu">
                           <li><a href="<?php echo SITE_URL; ?>/what-we-offer.php">What We Offer</a></li>
                            <li><a href="#">Promotion</a></li>
                            <li><a href="<?php echo SITE_URL; ?>/packages">Packages</a></li>
                          
                        </ul>
                    </li>

        <li><a href="<?php echo SITE_URL; ?>/service-availability">Service Availability</a></li>
        <!--  <li><a href="support.php">Support</a></li> -->
        <li><a href="<?php echo SITE_URL; ?>/contact-us">Contact Us</a></li>
      
      </ul>
     
     
    </div><!-- /.navbar-collapse -->
 <!--  <div class="header-bg"></div> -->
 </div>
              </div><!-- /.container -->
</nav>
      
      </header>
<!--    // end header -->

</div>