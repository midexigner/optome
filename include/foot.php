  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script type="text/javascript"  src="assets/js/vendor/jquery-3.2.1.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript"  src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript"  src="assets/js/validator.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.maskedinput.js"></script>
    <!-- <script src="assets/owlcarousel/owl.carousel.min.js"></script> -->
    
  <script type="text/javascript" src="assets/slick/slick.min.js"></script>
   <!--  <script type="text/javascript"  src="assets/swipper/swiper.js"></script> -->
    <script type="text/javascript"  src="assets/js/xsslider.js"></script>
    <script type="text/javascript"  src="assets/js/jquery.drawsvg.min.js"></script>
    <script type="text/javascript"  src="assets/js/wow.min.js"></script>
    <script type="text/javascript"  src="assets/js/main.js"></script>
    <script>

       $(document).ready(function(){
      $('.swiper-wrapper').slick({
  dots: false,
  infinite: true,
  arrows:false,
  slidesToShow: 4,
  slidesToScroll: 4,
   autoplay: true,
  autoplaySpeed: 2000,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
       
      }
    },
    {
      breakpoint: 800,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
      });
    });
 /*   var swiper = new Swiper('.swiper-container', {
      slidesPerView: 3,
      //spaceBetween: 100,
      freeMode: true,
      loop: true,
      //centeredSlides: true,
      autoplay:1000,
      
    });
       $('.swiper-container').on('mouseenter', function(e){
    console.log('stop autoplay');
    swiper.stopAutoplay();
  })
  $('.swiper-container').on('mouseleave', function(e){
    console.log('start autoplay');
    swiper.startAutoplay();
  })*/
    </script>


  </body>
</html>