     <?php 
 $query="SELECT ID,Website,Email,Phone,Mobile,Fax,Address,FooterAbout FROM settings WHERE ID <>0";
    $result = mysql_query ($query) or die(mysql_error()); 
    $num = mysql_num_rows($result);
   $row=mysql_fetch_array($result);
   $Website = $row["Website"];
   $EEmail = $row["Email"];
   $Phones = $row["Phone"];
   $Mobile = $row["Mobile"];
   $Fax = $row["Fax"];
   $Address = $row["Address"];
   $FooterAbout = $row["FooterAbout"];

                     ?><!-- // being footer -->
<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-3  col-sm-3 col-xs-3">
     <div class="footer-logo">
          <img src="assets/img/footer-logo.png" class="img-responsive" alt="footer-logo">
     </div>
      </div>
      <div class="col-md-3  col-sm-3 col-xs-3">
        <h3>Quick Links</h3>
        <ul class="list-unstyle">
        <li><a href="<?php echo SITE_URL; ?>/company">Company</a></li>
        <li><a href="<?php echo SITE_URL; ?>/service-availability">Service Availability</a></li>
        <li><a href="<?php echo SITE_URL; ?>/contact-us">Contact Us</a></li>
        <li><a href="<?php echo SITE_URL; ?>/login">Login</a></li>
        </ul>
      </div>
      <div class="col-md-3  col-sm-3 col-xs-3">
        <h3>Services</h3>
        <ul class="list-unstyle">
           <li><a href="<?php echo SITE_URL; ?>/what-we-offer.php">What We Offer</a></li>
          <li><a href="javascript:void(0)">Promotion</a></li>
          <li><a href="<?php echo SITE_URL; ?>/packages">Packages </a></li>
          </ul>
      </div>
       <div class="col-md-3  col-sm-3 col-xs-3">
         <h3>Contact Us</h3>
         <ul class="contact-info">
              <li><i class="fa fa-map-marker" aria-hidden="true"></i><?php echo $Address; ?></li>
              <li><i class="fa fa-phone" aria-hidden="true"></i><?php echo $Mobile; ?></li>
              <li><i class="fa fa-envelope-o" aria-hidden="true"></i><?php $eemail = explode(',', $EEmail);  
           
            ?><!-- -->

                        <a href="mailto:<?php echo @$eemail[0]; ?>"><?php echo @$eemail[0]; ?></a> , <a href="<?php echo @$eemail[1]; ?>"><?php echo @$eemail[1]; ?></a></li>
            </ul>

          
       </div>
    </div>
  </div>
</footer>

<div class="copyright">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-sm-12">
        <p>&copy; Copyright <?php echo date('Y'); ?> <a href="#">OPTOME. Powered By Connect. </a> All Rights Reserved</p>
      </div>
      <div class="col-md-6 text-right col-sm-12">
        <p>Designed and Developed By <a href="http://cloud-innovator.com/" target="_blank">Cloud Innovators Solution</a></p>
      </div>
    </div>
  </div>
</div>
<!-- // end footer -->