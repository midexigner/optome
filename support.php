<?php 
require_once 'admin/Common.php';
require_once 'include/head.php';
require_once 'include/header.php';
?>

<section id="apus-breadscrumb" class="apus-breadscrumb faq-breadscrumb">
         <div class="container">
            <div class="row">
               <div class="wrapper-breads">
                  <div class="breadscrumb-inner">
                     <ol class="breadcrumb">
                        <li><a href="index.php">Home</a>  </li>
                        <li class="active">Support</li>
                     </ol>
                     <h2 class="bread-title">Support</h2>
                  </div>
               </div>
            </div>
         </div>
      </section>


<section id="faq-blog" class="faq-blog-section">
         <div class="container">
            <div class="row section-content-2">
               <!-- //title -->
               <div class="faq-blog-content">
                  <div class="row">
                    
                     <div class="col-sm-12">
                        <div class="faq-blog-right margin-left-30"
                           <div class="kc-elm kc-css-466208 kc-title-wrap  left dark">
                              <div class="title-wrapper" role="tab" id="headingOne">
                                 <h3 class="kc_title"><span>OPTOME</span> F.A.Q.</h3>
                              </div>
                           </div>
                           <div class="kc-elm kc-css-601954 kc_text_block">
                              <p>We are accessible on call 24x7 and get your call with in 3 rings. Continuously feel like you are a part of the OPTOME family by getting personalized customer service. Time is money; don't invest it on hold with Time Warner.</p>
                           </div>
                           <div class="search-form padding-top-35">
                              <form action="#" method="get">
                                 <div class="input-group">
                                    <input type="text" placeholder="Search keywords" name="s" class="apus-search form-control input-sm">
                                    <span class="input-group-btn">
                                    <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                                    </span>
                                 </div>
                                 <input type="hidden" name="post_type" value="post" class="post_type">
                              </form>
                           </div>
                           <!-- /serch-bar -->

                           <br>
                           <br>
                           <br>
                           <div class="faq-content" >


<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title acc-btn">
        <a role="button" class="selected" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          What is Optome Fibre??
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body acc-content-inner">
         <div class="tab-content">
        <p>It's our fibre optic broadband service, offering superfast download speeds of up to 76Mbps.</p>

 <p>The actual speed you'll get depends on the package you choose, the quality of your phone line and your home wiring.</p>
</div>
      </div>
    </div>
  </div>
  <div class="panel">
    <div class="panel-heading" role="tab" id="headingTwo">
      <h4 class="panel-title acc-btn">
        <a class="collapsed" class="selected" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Will speeds go up and down?
        </a>
      </h4>
    </div>
    
    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
      <div class="panel-body acc-content-inner">
         <div class="tab-content">
      <p>Yes, fibre broadband uses the same technology (Dynamic Line Management) as standard broadband to give you the best possible service.</p>
                        <p>You'll probably see your speed vary over the first 10 days, as the broadband system runs tests to find the best speed for your service. This can cause your speeds to go up and down. You may even get disconnected a few times. Don't worry, this doesn't mean there's a problem, so please bear with it.</p>
     </div>
    </div>
    </div>
  </div>


  <div class="panel">
    <div class="panel-heading" role="tab" id="headingThree">
      <h4 class="panel-title acc-btn">
        <a class="collapsed" class="selected" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseTwo">
          Fibre availability: How can I find out if it's available in my area?
        </a>
      </h4>
    </div>
    
    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body acc-content-inner">
         <div class="tab-content">
     <p>Go to our homepage and click 'Check Availability' on any of our broadband products. Enter your landline phone and/or address details and we can let you know about fibre availability in your area.</p>
                        <p>Note: There are some reasons why you may not be able to get Plusnet Fibre, even though you live in a fibre optic broadband enabled area.</p>
                        <ol>
                            <li><i class="mn-icon-21"></i> If you're connected directly to a telephone exchange, rather than via a green cabinet</li>
                            <li><i class="mn-icon-21"></i> If your line is too far from your nearest green cabinet to support a stable fibre optic broadband service</li>
                            <li><i class="mn-icon-21"></i> Work hasn't yet been done at your nearest green cabinet</li>
                            <li><i class="mn-icon-21"></i> Your green cabinet isn't suitable for fibre optic cabling</li>
                        </ol>
      </div>
     
    </div>
    </div>
  </div>



    <div class="panel">
    <div class="panel-heading" role="tab" id="headingFour">
      <h4 class="panel-title acc-btn">
        <a class="collapsed" class="selected" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
         How fast will my broadband be?
        </a>
      </h4>
    </div>
    
    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
      <div class="panel-body acc-content-inner">
         <div class="tab-content">
     <p>You'll get the fastest speed that your telephone line can reliably support. Select the service you're interested in and click 'start your order' to get a speed estimate. Speeds are available as follows:</p>
                    <ul>
                     <li><i class="mn-icon-21"></i> Up to 38Mbps or Up to 76Mbps for Fibre (FTTC) enabled areas depending on the package you choose</li>
                        <li><i class="mn-icon-21"></i> Up to 17Mbps for ADSL2+ enabled areas</li>
                        <li><i class="mn-icon-21"></i> Up to 7Mbps service in other areas</li>
                    </ul>
                    <p>Your speed will be presented as a range of values in megabits per second (Mbps). The actual speed you get will usually be within these values and may vary depending on line conditions. If you find that your speed is much slower than the lowest estimated value you should call us on <strong>0800 432 0200</strong> and we'll work with you to get the best possible speed for your line.</p>
      </div>
     
    </div>
    </div>



  </div>


      <div class="panel">
    <div class="panel-heading" role="tab" id="headingFive">
      <h4 class="panel-title acc-btn">
        <a class="collapsed" class="selected" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
         Is support included?
        </a>
      </h4>
    </div>
    
    <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
      <div class="panel-body acc-content-inner">
         <div class="tab-content">
                      <p>We provide award-winning support 365 days a year, either online or over the phone. It's free to call from any UK landline or mobile.</p>
                        <p>There are a number of ways to contact us. Our help articles, FAQs sand Community Site forums</a> will also give you quick answers to your questions.</p>

               </div>
     
    </div>
    </div>


    
  </div>





  
</div>


                            
                           </div>
                           
                       
                        </div>
                     </div>
                     <!-- /col-sm-9 -->
                  </div>
               </div>
            </div>
         </div>
      </section>

<?php 

require_once 'include/footer.php';
require_once 'include/foot.php';

 ?>
  