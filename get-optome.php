<?php 
require_once 'admin/Common.php';
require_once 'include/head.php';
require_once 'include/header.php';
$packagesHeadBgQ = mysql_query('SELECT * FROM `banners` WHERE ID = 11');
$packageHeadRowBg = mysql_fetch_assoc($packagesHeadBgQ);
?>

<style>

  .get-optome-breadscrumb {
    background-image: url(<?php echo SITE_URL .'/admin/'.DIR_BANNERS.$packageHeadRowBg['Image'] ?>) !important;
}
</style>

<section id="apus-breadscrumb" class="apus-breadscrumb get-optome-breadscrumb">
         <div class="container">
            <div class="row">
               <div class="wrapper-breads">
                  <div class="breadscrumb-inner">
                     <ol class="breadcrumb">
                        <li><a href="index.php">Home</a>  </li>
                        <li class="active">Get OptoMe</li>
                     </ol>
                     <h2 class="bread-title">Get OptoMe</h2>
                  </div>
               </div>
            </div>
         </div>
      </section>

<section id="get-optome" class="contact-section section-padding">
  <div class="section-paddings">
       <div class="container">
        <div class="row">
                <div class="col-md-12">
                  <div class="page-header section-header text-center">
                    <h1 class="h-light">Awesome</h1>
                    <h1 class="h-bold">Get <span>OptoMe</span></h1><br>
                    <span class="line text-center"></span><br>
                    <p>In search of a broadband that guarantees reliability? </p>
                    <p>Type in your details below and we will cater you.</p>
                  </div>
                </div>
              </div>
    <div class="row">
    <div class="col-md-12">
                    
<form action="" method="post" autocomplete="off">
<div class="type-detail">
   
   <div class="question">Hi, My name is 
    <div class="name question_name">
        <input type="text" name="travel_name" id="name" class="required-entry" value="" maxlength="128">
    </div>
</div>

<div class="question "  id="dob">My date of birth is
<div class="travel_my_dob dates" id="my_dob" data-dob="">
<div id="my_dob_years" class="select-box select-years">
    <select id="my_dob_year">
        <option value="">Please Select</option>
        <?php for($i=1952;$i<=date('Y');$i++)
{ ?>
        <option value="<?php echo $i ?>" class="years_<?php echo $i ?>"><?php echo $i ?></option>
        <?php } ?>
        
    </select>
    </div>
<div id="travel_my_dob_months" class="select-box select-months">
    <select id="travel_my_dob_month">
        <?php  for ($i = 1; $i <= 12; $i++)
            {$month_name = date('F', mktime(0, 0, 0, $i, 1, 2011)); ?>
        <option value="<?php echo $i; ?>" class="months_<?php echo $i; ?>"><?php echo $month_name; ?></option>
        <?php } ?>
       
    </select>
<div id="travel_my_dob_days" class="select-box select-days">
    <select id="travel_my_dob_day">
        <?php  for ($i = 1; $i <= 31; $i++)
            {?>
        <option value="<?php echo $i; ?>" class="days_<?php echo $i; ?>"><?php echo $i; ?></option>
          <?php } ?>
        </select></div>

</div>

</div>


    
</div>
<br>
  <div class="question " id="contactNumber">My Contact Number is 
    <div class="name question_name">
        <input type="text" name="number" id="number" class="required-entry" value="" maxlength="128">
    </div>
</div> 
<br>
  <div class="question emailAddress">My Email Address is 
    <div class="name question_name">
        <input type="email" name="email" id="email" class="required-entry" value="" maxlength="128">
    </div>
</div> 
<br>
  <div class="question Address">My Address is 
    <div class="name question_name">
        <input type="text" name="address" id="address" class="required-entry" value="" maxlength="128">
    </div>
</div> 
<br>
<div class="question interestedAddon " >I am interested in
<div class="travel_my_dob dates" id="travel_my_dob" data-dob="">
<div id="travel_my_dob_years" class="select-box select-years">
    <select id="travel_my_dob_year">
        <option value="">Please Select Packages</option>
        <option value="single_5" >Single 5</option>
        <option value="double_1" >Double 1</option>
        <option value="double_2" >Double 2</option>
        <option value="double_3" >Double 3</option>
        <option value="double_4" >Double 4</option>
        <option value="double_5" >Double 5</option>
        <option value="double_6" >Double 6</option>
        <option value="double_7" >Double 7</option>
        <option value="double_8" >Double 8</option>
        <option value="double_9" >Double 9</option>
    </select>
    </div>
   <span> Add-on</span>
<div id="travel_my_dob_months" class="select-box select-months">
    <select id="travel_my_dob_month">
        <option value="skip" class="months">Skip</option>
        <option value="Tv_Phone" class="months">Tv & Phone</option>
        <option value="Phone" class="months">Tv</option>
        <option value="Phone" class="months">Phone</option>
      
       
    </select>


</div>

</div>


    
</div>

<br>
<div class="question call " >When to call From
<div class="my_call dates" id="my_call" data-dob="">
<div id="my_from_time" class="select-box select-years">
  <select id="from_time">
        <option value="">Please Select Time</option>
        <option value="as_soon_as_possible" >As Soon As Possible</option>
        <option value="single_5" >10:00 AM</option>
        <option value="double_1" >11:00 AM</option>
        <option value="double_2" >12:00 PM</option>
        <option value="double_3" >01:00 PM</option>
        <option value="double_4" >02:00 PM</option>
        <option value="double_5" >03:00 PM</option>
        <option value="double_6" >04:00 PM</option>
        <option value="double_7" >05:00 PM</option>
        <option value="double_8" >06:00 PM</option>
        <option value="double_9" >07:00 PM</option>
        <option value="double_9" >08:00 PM</option>
        <option value="double_9" >09:00 PM</option>
        <option value="double_9" >10:00 PM</option>

        
    </select>
    </div>
    
<div id="my_to_time" class="select-box select-months">
  To
  <select id="to_time">
        <option value="">Please Select Time</option>
        <option value="single_5" >10:00 AM</option>
        <option value="double_1" >11:00 AM</option>
        <option value="double_2" >12:00 PM</option>
        <option value="double_3" >01:00 PM</option>
        <option value="double_4" >02:00 PM</option>
        <option value="double_5" >03:00 PM</option>
        <option value="double_6" >04:00 PM</option>
        <option value="double_7" >05:00 PM</option>
        <option value="double_8" >06:00 PM</option>
        <option value="double_9" >07:00 PM</option>
        <option value="double_9" >08:00 PM</option>
        <option value="double_9" >09:00 PM</option>
        <option value="double_9" >10:00 PM</option>
       
        
    </select>


</div>

</div>


    
</div>
<br>
<div class="question submit " >
<div class="travel_my_dob dates" id="travel_my_dob" data-dob="">
<div id="travel_my_dob_years" class="select-box select-years">
    <input type="submit" name="submit" class="btn btn-theme" id="submit" value="Send Now" >
    </div>
</div>


    
</div>
</form>

                          










   

</div> 
                </div>
            </div>
</div>


          </section>

       

<?php 

require_once 'include/footer.php';
require_once 'include/foot.php';
 ?>

 <script src="assets/js/order-form.js"></script>
 <script type="text/javascript">
   $('#name').keyup(function(){
        $('#dob').fadeIn();
     });
     $('#my_dob_year').change(function(){
        $('#contactNumber').fadeIn();
     });
     $('#number').keyup(function(){
        $('.emailAddress').fadeIn();
     });
     $('#email').keyup(function(){
        $('.Address').fadeIn();
     });
     $('#address').keyup(function(){
        $('.interestedAddon').fadeIn();
     });
      $('.interestedAddon').change(function(){
        $('.call').fadeIn();
     });
       $('#from_time').change(function(){
        $('.submit').fadeIn();
     });

          $('#from_time').on('change', function() {

  if($('#from_time').val() == 'as_soon_as_possible') {
            $('#my_to_time').fadeOut();; 
        } else {
            $('#my_to_time').fadeIn();; 
        } 

})

 </script>



  